const tsConfig = require("./tsconfig.json");

const alias = {};

Object.keys(tsConfig.compilerOptions.paths).forEach((key) => {
  alias[key] = tsConfig.compilerOptions.paths[key][0];
});

const plugins = [
  [
    "module-resolver",
    {
      alias,
    },
  ],
];

// .babelrc.js exports
module.exports = {
  plugins,
};