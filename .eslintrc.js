module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: "./tsconfig.json",
    tsconfigRootDir: __dirname,
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: ["@typescript-eslint", "react-hooks", "prettier", "jam3"],
  extends: [
    "plugin:react/recommended",
    "plugin:@typescript-eslint/recommended",
    "standard",
    "standard-with-typescript",
    "prettier",
  ],
  rules: {
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "react/prop-types": "off",
    semi: [2, "always"],
    "no-unused-vars": ["warn"],
    "no-use-before-define": "off",
    "@typescript-eslint/no-use-before-define": ["error"],
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/strict-boolean-expressions": "off",
    "@typescript-eslint/consistent-type-definitions": ["error", "interface"],
    "prettier/prettier": "warn",
    "@typescript-eslint/triple-slash-reference": "warn",
    "@typescript-eslint/no-namespace": "off",
    "n/no-callback-literal": "off",
    "@typescript-eslint/prefer-nullish-coalescing": "warn",
    "jam3/no-sanitizer-with-danger": [
      2,
      {
        wrapperName: ["sanitizer"],
      },
    ],
    complexity: ["error", { max: 10 }],
    curly: "error",
  },
  globals: {
    NodeJS: true,
    Drupal: true,
    drupalSettings: true,
  },
  settings: {
    react: {
      pragma: "React",
      version: "detect",
    },
  },
  env: {
    browser: true,
    jest: true,
  },
};
