### _src/components_

## Components folder

We use this folder to organise all components in our project.

## Logic behind this folder structure

For this project we have the following structure:

### rendering components

- **factories**: these are the components that are used to create other components
- **portals**: these are the components that are used to render factories in specific entrypoints in the DOM

### search components

- **widgets**: these are the components that implement search functionality
