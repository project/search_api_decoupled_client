import React from "react";

import { configService } from "@services";

import PortalFactory from "./portalFactory";

const AppFactory = () => {
  const { appKeys, appIds, appPagerTypes, appAvoidEmptyQueryConfig } =
    configService.useConfig();

  // we might need to have a portal layout config that for example renders:
  // on a flex row or flex column, specifies the gap between them and so on
  // maybe it's better to have that on the portal level components/portals/portal.tsx

  return (
    <>
      {appKeys.map((appKey, i) => (
        <configService.AppConfigContextProvider
          appKey={appKey}
          appId={appIds[i]}
          pagerType={appPagerTypes[i]}
          avoidEmptyQuery={appAvoidEmptyQueryConfig[i]}
          key={appKey}
        >
          <PortalFactory />
        </configService.AppConfigContextProvider>
      ))}
    </>
  );
};

export default AppFactory;
