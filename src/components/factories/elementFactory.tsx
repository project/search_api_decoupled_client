import React from "react";

import { configService } from "@services";
import {
  type CustomResultsSettingsType,
  type ConfigElementType,
  type RenderedResultsSettingsType,
  type SorterSettingsType,
} from "@types";
import {
  AutocompleteInterface,
  ActiveFiltersInterface,
  ExposedFilterInterface,
  FacetInterface,
  LoadMoreInterface,
  PagerInterface,
  RefreshInterface,
  RenderedResults,
  RenderedSwitchResults,
  ResetInterface,
  ResultsInterface,
  InputInterface,
  SummaryInterface,
  SorterInterface,
} from "@components/interfaces";

const ElementFactory = (props: { portalKey: string }) => {
  const { appElementsByPortal } = configService.useConfig();
  const elements = appElementsByPortal(props.portalKey);

  if (elements) {
    return (
      <>
        {(elements as ConfigElementType[]).map(
          // eslint-disable-next-line complexity
          (element, i) => {
            switch (element?.type) {
              //! CUSTOM ELEMENTS
              //* there are different widget types for each element we split them into different factories
              case "facet":
                return <FacetInterface {...element} key={element.id} />;
              case "exposed_filter":
                return <ExposedFilterInterface {...element} key={element.id} />;
              case "pager":
                return <PagerInterface {...element} key={element.id} />;
              case "loadmore":
                return <LoadMoreInterface {...element} key={element.id} />;
              case "search_input":
                return <InputInterface {...element} key={element.id} />;
              case "autocomplete":
                return <AutocompleteInterface {...element} key={element.id} />;
              case "summary":
                return <SummaryInterface {...element} key={element.id} />;
              case "search_result_custom":
                return (
                  <ResultsInterface
                    {...(element as ConfigElementType<CustomResultsSettingsType>)}
                    key={element.id}
                  />
                );
              case "reset_filters":
                return <ResetInterface {...element} key={element.id} />;
              case "refresh_results":
                return <RefreshInterface {...element} key={element.id} />;
              case "sorting":
                return (
                  <SorterInterface
                    {...(element as ConfigElementType<SorterSettingsType>)}
                    key={element.id}
                  />
                );
              case "active_filters":
                return <ActiveFiltersInterface {...element} key={element.id} />;

              //! SPECIAL ELEMENTS
              //* these are special elements, main difference should be that they receive rendered html
              case "search_result":
                return (
                  <RenderedResults
                    {...(element as ConfigElementType<RenderedResultsSettingsType>)}
                    key={element.id}
                  />
                );
              case "search_result_switch":
                return (
                  <RenderedSwitchResults
                    {...(element as ConfigElementType<RenderedResultsSettingsType>)}
                    key={element.id}
                  />
                );
              default:
                return <div key={`not found ${i}`}>Element NOT FOUND</div>;
            }
          }
        )}
      </>
    );
  } else {
    return <></>;
  }
};

export default ElementFactory;
