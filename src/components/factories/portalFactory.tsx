import React from "react";

import { configService } from "@services";
import { Portal } from "@components/portals";

const PortalFactory = () => {
  // todo: useConfig and get the portals list
  const { appPortalKeys } = configService.useConfig();

  // we might need to have a portal layout config that for example renders:
  // on a flex row or flex column, specifies the gap between them and so on
  // maybe it's better to have that on the portal level components/portals/portal.tsx

  return (
    <>
      {appPortalKeys?.map((portalKey) => (
        <Portal id={portalKey} key={portalKey} />
      ))}
    </>
  );
};

export default PortalFactory;
