import React from "react";
import { createPortal } from "react-dom";

import { configService } from "@services";

import { ElementFactory } from "@components/factories";

const Portal = (props: { id: string }) => {
  const appKey = configService.useAppKey() ?? "default";

  const errorEl = <> Couldn&apos;t find the entrypoint for {props.id} </>;

  try {
    const portalEl = document.querySelector(
      `[data-block-id=${appKey}] .${props.id}`
    );

    if (portalEl) {
      return createPortal(
        // we might need to have a portal layout config that for example renders:
        // on a flex row or flex column, specifies the gap between them and so on
        // something like <PortalLayout config={portalConfig}><ElementFactory portalId={props.id} /></PortalLayout>

        // Each portal renders elements inside of it
        <ElementFactory portalKey={props.id} key={props.id} />,
        portalEl
      );
    } else {
      return errorEl;
    }
  } catch (e) {
    return errorEl;
  }
};

export default Portal;
