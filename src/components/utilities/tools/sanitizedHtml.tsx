import React, { forwardRef } from "react";

import { useSanitizer } from "@utils";

const SanitizedHTML = forwardRef(function Sanitized(
  props: {
    children: string;
    onClick?: () => void;
    slot?: string;
  },
  ref?: React.Ref<HTMLDivElement>
) {
  const sanitizer = useSanitizer();

  return (
    <div
      slot={props.slot}
      ref={ref}
      onClick={props.onClick}
      dangerouslySetInnerHTML={{
        __html: sanitizer(
          props.children,
          // allow any custom element
          {
            CUSTOM_ELEMENT_HANDLING: {
              // allow any tag name on custom elements
              tagNameCheck: /^/,
              attributeNameCheck: /^/,
              allowCustomizedBuiltInElements: true,
            },
            ADD_ATTR: [
              "mode",
              "preview_image",
              "category",
              "url",
              "title",
              "placeholder",
              "value",
              "test",
              "snippet",
              "loading",
              "initial-value",
              "facet",
              "show-count",
              "orientation",
              "resettable",
              "reset-text",
              "show-label",
              "label",
              "categories",
              "initial-whitespace",
              "autofocus",
              "children",
              "text",
              "key",
              "count",
              "switchable",
              "columns",
              "loading-text",
              "no-results-text",
              "pages",
              "current-page",
              "number-of-results",
              "time-taken",
              "active",
              "slot",
            ],
          }
        ),
      }}
    ></div>
  );
});

export default SanitizedHTML;
