import React, { useCallback, useRef } from "react";

import { type ConfigElementType, type ExposedFilterSettingsType } from "@types";
import { searchService } from "@services";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! FILTER WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED (there will be 1 or 2 elements with these attributes):
  ? value: string
  ? from-value: string
  ? to-value: string
  ? operator: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true)
  ? filter-change: {detail: { from?: string, to?: string, value?: string }} 
  ? ^ (if not provided the value will not be muted, "value" will override from and to)
  ? reset: {} 
  ? ^ (use it in order to reset the whole filter)
*/

const ExposedFilterInterface = (
  element: ConfigElementType<ExposedFilterSettingsType>
) => {
  const ref = useRef<HTMLDivElement>(null);
  const { filterState, setExposedFilter, isLoading, isFetching } =
    searchService.useSearch({
      filterName: element.settings?.field_name,
    });

  // Handlers
  const onChange = useCallback(
    (e: Event) => {
      const values = (e as CustomEvent).detail;
      setExposedFilter({ ...values, operator: element.settings.operator });
    },
    [setExposedFilter, element.settings.operator]
  );

  const onReset = useCallback(() => {
    setExposedFilter({});
  }, [setExposedFilter]);

  // Listeners
  useCustomEventListeners(ref, {
    "filter-change": onChange,
    reset: onReset,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: !!filterState?.value || !!filterState?.min || !!filterState?.max,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        operator={element.settings.operator}
        value={filterState?.value}
        from-value={filterState?.min}
        to-value={filterState?.max}
      />
    </CustomElementWrapperTag>
  );
};

export default ExposedFilterInterface;
