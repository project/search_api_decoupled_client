import React, { useCallback, useMemo, useRef, useState } from "react";

import {
  type RenderedResultsSettingsType,
  type ConfigElementType,
} from "@types";
import { searchService, configService } from "@services";
import {
  getWrapperId,
  useCustomElementTags,
  useWrapperAttributes,
  useCustomEventListeners,
  useResultsAttributes,
} from "@utils";

import { SanitizedHTML } from "@components/utilities";

/*
 ! SWITCHABLE RENDERED RESULTS INTERFACE
 
 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean
 -- specific
 ? no-results-text: string
 ? loading-text: string
 ? columns: number
 ? switchable: boolean (true)
 ? mode: "list" | "grid"

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? mode-change: {detail: {mode: "list" | "grid"}}
 */

const SearchResults = (
  element: ConfigElementType<RenderedResultsSettingsType>
) => {
  const ref = useRef<HTMLDivElement>(null);
  const { searchResults, isFetching, isLoading, allResults } =
    searchService.useSearch();
  const { pagerType } = configService.useAppConfig();

  const resultList = useMemo(() => {
    return pagerType === "loadmore" ? allResults : searchResults;
  }, [allResults, pagerType, searchResults]);

  const [mode, setMode] = useState<"list" | "grid">(
    ((element.settings.columns as number) ?? 0) > 1 ? "grid" : "list"
  );

  // Handlers
  const onModeChange = useCallback(
    (e: Event) => {
      console.log("mode change event", e);
      setMode((e as CustomEvent).detail.mode);
    },
    [setMode]
  );

  // Listeners
  useCustomEventListeners(ref, {
    "mode-change": onModeChange,
  });

  // Render
  const { CustomElementWrapperTag } = useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
  });
  const resultsAttributes = useResultsAttributes(element, { mode });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
      {...resultsAttributes}
    >
      {resultList?.map((result) => (
        <div key={result.id} data-element-type data-element-type-processed>
          <SanitizedHTML>
            {result[
              mode === "list"
                ? element.settings.field
                : element.settings.field_grid
            ]?.toString() ?? ""}
          </SanitizedHTML>
        </div>
      ))}
    </CustomElementWrapperTag>
  );
};

export default SearchResults;
