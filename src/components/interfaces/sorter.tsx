import React, { useCallback, useRef } from "react";

import { searchService } from "@services";
import { type ConfigElementType, type SorterSettingsType } from "@types";
import {
  useCustomElementTags,
  useWrapperAttributes,
  useSortingOptions,
  getElementId,
  getWrapperId,
  trueOrUndefined,
  useCustomEventListeners,
} from "@utils";

/*
 ! SORTER WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? label: string
 ? sort-by: string
 ? order-by: string
 ? active: boolean

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? sort-search: {detail: { sort_by, status }}
 */

const SorterInterface = (element: ConfigElementType<SorterSettingsType>) => {
  const ref = useRef<HTMLDivElement>(null);
  const { setSorting, currentSorting, isLoading, isFetching } =
    searchService.useSearch();

  const sortingOptions = useSortingOptions(element);

  // Handlers
  const onSort = useCallback(
    (e: Event) => {
      const { sortBy, orderBy } = (e as CustomEvent).detail;
      setSorting(sortBy, orderBy);
    },
    [setSorting]
  );

  // Listeners
  useCustomEventListeners(ref, {
    "sort-search": onSort,
  });

  // CustomElementTag
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: !!currentSorting?.sortBy || !!currentSorting?.orderBy,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      {sortingOptions.map((option) => (
        <CustomElementTag
          key={option.key}
          data-element-type={getElementId(element)}
          label={option.label}
          sort-by={option.key}
          order-by={option.order}
          active={trueOrUndefined(
            currentSorting?.sortBy === option.key &&
              currentSorting?.orderBy === option.order
          )}
        />
      ))}
    </CustomElementWrapperTag>
  );
};

export default SorterInterface;
