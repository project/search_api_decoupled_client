import React, { useMemo } from "react";

import {
  type ConfigElementType,
  type RenderedResultsSettingsType,
} from "@types";
import { searchService, configService } from "@services";
import {
  getWrapperId,
  useCustomElementTags,
  useWrapperAttributes,
  useResultsAttributes,
} from "@utils";

import { SanitizedHTML } from "@components/utilities";

/*
 ! RENDERED RESULTS INTERFACE
 
 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean
 -- specific
 ? columns: number
 ? mode: "list" | "grid"
 ? no-results-text: string
 ? loading-text: string
 */

const SearchResults = (
  element: ConfigElementType<RenderedResultsSettingsType>
) => {
  const { searchResults, isFetching, isLoading, allResults } =
    searchService.useSearch();
  const { pagerType } = configService.useAppConfig();

  const resultList = useMemo(() => {
    return pagerType === "loadmore" ? allResults : searchResults;
  }, [allResults, pagerType, searchResults]);

  // Render
  const { CustomElementWrapperTag } = useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
  });
  const resultsAttributes = useResultsAttributes(element);

  return (
    <CustomElementWrapperTag
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
      {...resultsAttributes}
    >
      {resultList?.map((result) => (
        <div
          key={result.id}
          data-element-type
          data-element-type-processed="true"
        >
          <SanitizedHTML>
            {result[element.settings.field]?.toString() ?? ""}
          </SanitizedHTML>
        </div>
      ))}
    </CustomElementWrapperTag>
  );
};

export default SearchResults;
