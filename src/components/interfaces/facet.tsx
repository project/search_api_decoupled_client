import React, { useCallback, useMemo, useRef } from "react";

import { type FacetSettingsType, type ConfigElementType } from "@types";
import { searchService } from "@services";
import {
  getElementId,
  getWrapperId,
  trueOrUndefined,
  useCustomEventListeners,
  useCustomElementTags,
  useWrapperAttributes,
  useRangeAttributes,
  useDebouncedCallback,
} from "@utils";

/*
 ! FACET WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? facet: string
 if it's not range
 ? active: boolean
 ? count: number
 ? show-count: boolean
 ? facet-id: string
 ? label: string
 if is range
 ? min: number
 ? active-min: number
 ? max: number
 ? active-max: number
 ? step: number
 ? single: boolean
 ? lower-label: string
 ? higher-label: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true)
 ? facet-change: {detail: { key, active, reset}}
 ? facet-range-change: {detail: { min, max}}
 ? reset: {}
 */

const FacetInterface = (element: ConfigElementType<FacetSettingsType>) => {
  const ref = useRef<HTMLDivElement>(null);
  const {
    facetState,
    setFacetFilter,
    setRangeFacetFilter,
    isLoading,
    isFetching,
  } = searchService.useSearch({
    facetKey: element.settings?.facet,
    facetAlias: element.settings?.url_alias,
  });

  const isRange = useMemo(() => {
    return (
      element.settings.min_value !== undefined &&
      element.settings.step !== undefined
    );
  }, [element.settings.min_value, element.settings.step]);

  const checked = useMemo(
    () =>
      facetState?.results
        .filter((result) => result.active)
        .map((result) => result.key) ?? [],
    [facetState?.results]
  );

  const [currentMin, currentMax] = useMemo(() => {
    if (facetState?.active_values?.[0]?.length === 2) {
      return [
        Number(facetState?.active_values?.[0]?.[0]),
        Number(facetState?.active_values?.[0]?.[1]),
      ];
    }
    return [undefined, undefined];
  }, [facetState?.active_values]);

  // Handlers
  const onChange = useCallback(
    (e: Event) => {
      const { key, active, reset } = (e as CustomEvent).detail;
      const newChecked = reset ? [] : checked.filter((item) => item !== key);
      if (active) {
        newChecked.push(key);
      }
      setFacetFilter(newChecked);
    },
    [checked, setFacetFilter]
  );

  const onRangeChange = useDebouncedCallback(
    useCallback(
      (e: Event) => {
        const { min, max } = (e as CustomEvent).detail;
        setRangeFacetFilter({ min, max });
      },
      [setRangeFacetFilter]
    ),
    500
  );

  const onReset = useCallback(
    (e: Event) => {
      setFacetFilter([]);
    },
    [setFacetFilter]
  );

  // Listeners
  useCustomEventListeners(ref, {
    "facet-change": onChange,
    "facet-range-change": onRangeChange,
    reset: onReset,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: !!facetState?.results.length && !!checked.length,
  });
  const rangeAttributes = useRangeAttributes(element, facetState);

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      {isRange ? (
        <CustomElementTag
          data-element-type={getElementId(element)}
          {...rangeAttributes}
          active-lower={currentMin}
          active-higher={currentMax}
        />
      ) : (
        facetState?.results.map((result) => (
          <CustomElementTag
            data-element-type={getElementId(element)}
            active={trueOrUndefined(result.active)}
            facet-key={result.key}
            key={result.key}
            label={result.label}
            facet={element.settings.facet}
            show-count={trueOrUndefined(element.settings.show_numbers)}
            count={result.count}
          />
        ))
      )}
    </CustomElementWrapperTag>
  );
};

export default FacetInterface;
