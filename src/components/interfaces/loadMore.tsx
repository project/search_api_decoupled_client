import React, { useCallback, useRef } from "react";

import { type ConfigElementType } from "@types";
import { searchService } from "@services";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! LOADMORE WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? loading: boolean
 ? displayed: number
 ? total: number
 ? load-more-text: string
 ? middle-text: string
 ? unit-text: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? load: {}
 */

const LoadMore = (element: ConfigElementType) => {
  const ref = useRef<HTMLDivElement>(null);

  const {
    nOfResults,
    setPage,
    currentPage,
    allResults,
    isFetching,
    isLoading,
  } = searchService.useSearch();

  // Handlers
  const onLoadMore = useCallback(() => {
    const nextPage = currentPage + 1;
    setPage(nextPage.toString(), "loadmore");
  }, [currentPage, setPage]);

  // Listeners
  useCustomEventListeners(ref, {
    load: onLoadMore,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: currentPage > 0,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        loading={wrapperAttributes.loading}
        displayed={allResults?.length ?? 0}
        total={nOfResults}
        load-more-text={Drupal.t("Load More")}
        middle-text={Drupal.t("of")}
        unit-text={Drupal.t("results")}
      />
    </CustomElementWrapperTag>
  );
};

export default LoadMore;
