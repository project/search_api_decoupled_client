import React, { useState, useRef, useCallback } from "react";

import { searchService } from "@services";
import { type ConfigElementType, type InputSettingsType } from "@types";
import {
  getChildId,
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! AUTOCOMPLETE WIDGET INTERFACE
 ! if changed please update the wiki

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? value: string
 ? placeholder: string

 * SUGGESTION ATTRIBUTES PASSED: 
 * (suggestion component should be named <(custom-element)-suggestion/>)
 ? value: string
 ? label: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? input-change: {detail: { value }}
 ? input-submit: {detail: { value }}
 ? input-reset: {}

 * SUGGESTION EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? suggestion-click: {detail: { text }} 
 */

const Autocomplete = (element: ConfigElementType<InputSettingsType>) => {
  const ref = useRef<HTMLDivElement>(null);
  const { currentTextKeywords, setTextFilter, isFetching, isLoading } =
    searchService.useSearch();
  const [keywords, setKeywords] = useState(currentTextKeywords);
  const { suggestions } = searchService.useAutocomplete(keywords);

  // Handlers
  const onSubmit = useCallback(() => {
    setTextFilter(keywords);
  }, [setTextFilter, keywords]);

  const onChange = useCallback(
    (e: Event) => {
      setKeywords((e as CustomEvent).detail.value);
    },
    [setKeywords]
  );

  const onReset = useCallback(() => {
    setKeywords("");
    setTextFilter("");
  }, [setTextFilter]);

  const onSuggestionClick = useCallback(
    (e: Event) => {
      const { text } = (e as CustomEvent).detail;
      setKeywords(text);
    },
    [setKeywords]
  );

  // Listeners
  useCustomEventListeners(ref, {
    "input-change": onChange,
    "input-reset": onReset,
    reset: onReset,
    "input-submit": onSubmit,
    "suggestion-click": onSuggestionClick,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag, CustomElementChildTag } =
    useCustomElementTags(element, "suggestion");
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: keywords.length > 0,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        placeholder={element.settings?.placeholder ?? ""}
        value={keywords}
      >
        {suggestions?.map((suggestion) => (
          <CustomElementChildTag
            key={suggestion.value}
            data-element-type={getChildId(element)}
            text={suggestion.value}
            snippet={keywords}
          />
        ))}
      </CustomElementTag>
    </CustomElementWrapperTag>
  );
};

export default Autocomplete;
