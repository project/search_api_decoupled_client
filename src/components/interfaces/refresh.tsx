import React, { useCallback, useRef } from "react";

import { searchService } from "@services";
import { type ConfigElementType } from "@types";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! REFRESH WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? label: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? refresh-search: {}
 */

const RefreshInterface = (element: ConfigElementType) => {
  const ref = useRef<HTMLDivElement>(null);
  const { refetch, isLoading, isFetching } = searchService.useSearch();

  // Handlers
  const onRefresh = useCallback(() => {
    refetch().catch((e) => {
      console.warn(e);
    });
  }, [refetch]);

  // Listeners
  useCustomEventListeners(ref, {
    "refresh-search": onRefresh,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        label={element.settings.label}
      />
    </CustomElementWrapperTag>
  );
};

export default RefreshInterface;
