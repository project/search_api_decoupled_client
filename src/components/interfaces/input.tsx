import React, { useCallback, useRef, useState } from "react";

import { type ConfigElementType, type InputSettingsType } from "@types";
import { searchService } from "@services";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! SIMPLE INPUT WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? value: string
 ? placeholder: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? input-change: {detail: { value }}
 ? input-reset: {}
 ? input-submit: {}
 */

const InputInterface = (element: ConfigElementType<InputSettingsType>) => {
  const ref = useRef<HTMLDivElement>(null);
  const { currentTextKeywords, setTextFilter, isLoading, isFetching } =
    searchService.useSearch();
  const [keywords, setKeywords] = useState(currentTextKeywords);

  // Handlers
  const onChange = useCallback(
    (e: Event) => {
      const { value } = (e as CustomEvent).detail;
      setKeywords(value);
    },
    [setKeywords]
  );

  const onReset = useCallback(() => {
    setKeywords("");
    setTextFilter("");
  }, [setTextFilter]);

  const onSubmit = useCallback(() => {
    setTextFilter(keywords);
  }, [keywords, setTextFilter]);

  // Listeners
  useCustomEventListeners(ref, {
    "input-change": onChange,
    "input-reset": onReset,
    "input-submit": onSubmit,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: !!currentTextKeywords,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        placeholder={element.settings?.placeholder ?? ""}
        value={currentTextKeywords}
      />
    </CustomElementWrapperTag>
  );
};

export default InputInterface;
