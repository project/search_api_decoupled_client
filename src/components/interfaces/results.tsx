import React, { useCallback, useMemo, useRef, useState } from "react";

import { type ConfigElementType, type CustomResultsSettingsType } from "@types";
import { searchService, configService } from "@services";
import {
  encodeText,
  getElementId,
  getWrapperId,
  useAbsolutePath,
  useCustomElementTags,
  useCustomEventListeners,
  useResultsAttributes,
  useWrapperAttributes,
} from "@utils";

import { SanitizedHTML } from "@components/utilities";

/*
 ! FACET WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean
 -- specific
 ? no-results-text: string
 ? loading-text: string
 ? columns: number
 ? switchable: boolean
 ? mode: "list" | "grid"

 * ATTRIBUTES PASSED:
 ? category: string
 ? title: string
 ? snippet: string
 ? excerpt: string
 ? text: string (html allowed)
 ? url: string
 ? date: string
 ? preview_image: string

 * SLOTS
 ? text: string (html allowed)

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? mode-change: {detail: {mode: "list" | "grid"}}
 */

const ResultsInterface = (
  element: ConfigElementType<CustomResultsSettingsType>
) => {
  const ref = useRef<HTMLDivElement>(null);

  const {
    searchResults,
    isFetching,
    isLoading,
    allResults,
    currentTextKeywords,
  } = searchService.useSearch();
  const { pagerType } = configService.useAppConfig();
  const getAbsolutePath = useAbsolutePath();

  const resultList = useMemo(() => {
    return pagerType === "loadmore" ? allResults : searchResults;
  }, [allResults, pagerType, searchResults]);

  const [mode, setMode] = useState<"list" | "grid">(
    ((element.settings.columns as number) ?? 0) > 1 ? "grid" : "list"
  );

  // Handlers
  const onModeChange = useCallback(
    (e: Event) => {
      setMode((e as CustomEvent).detail.mode);
    },
    [setMode]
  );

  // Listeners
  useCustomEventListeners(ref, {
    "mode-change": onModeChange,
  });

  // Render
  const { CustomElementWrapperTag, CustomElementTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
  });
  const resultsAttributes = useResultsAttributes(element, { mode });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
      {...resultsAttributes}
    >
      {resultList?.map((result) => (
        <CustomElementTag
          key={result.id}
          data-element-type={getElementId(element)}
          category={result?.[element.settings.field_category ?? "type"]}
          title={result?.[element.settings.field_title ?? "title"]}
          text={encodeText(
            (
              result?.[element.settings.field_text ?? "preview_text"] ?? ""
            ).toString()
          )}
          preview_image={getAbsolutePath(
            result?.[element.settings.field_image ?? "preview_image"] as string
          )}
          date={result?.[element.settings.field_date]}
          excerpt={result.excerpt}
          snippet={currentTextKeywords}
          url={result.url}
          mode={mode}
        >
          <SanitizedHTML slot="text">
            {(result?.[
              element.settings.field_text ?? "preview_text"
            ] as string) ?? ""}
          </SanitizedHTML>
        </CustomElementTag>
      ))}
    </CustomElementWrapperTag>
  );
};

export default ResultsInterface;
