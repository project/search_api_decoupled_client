import React, { useRef } from "react";

import { type ConfigElementType } from "@types";
import { searchService } from "@services";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! PAGER WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? current-page: number
 ? pages: number

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? page-change: {detail: number}
 */

const Pager = (element: ConfigElementType) => {
  const ref = useRef<HTMLDivElement>(null);
  const { nOfPages, currentPage, setPage, isFetching, isLoading } =
    searchService.useSearch();

  // Handlers
  const onPageChange = (e: Event) => {
    const page = (e as CustomEvent).detail;
    setPage(page, "pager");
  };

  // LISTERNERS
  useCustomEventListeners(ref, {
    "page-change": onPageChange,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: currentPage > 0,
  });

  if (nOfPages <= 1) {
    return null;
  }

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        current-page={currentPage}
        pages={nOfPages}
      />
    </CustomElementWrapperTag>
  );
};

export default Pager;
