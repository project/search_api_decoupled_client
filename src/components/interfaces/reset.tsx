import React, { useRef } from "react";

import { searchService } from "@services";
import { type ConfigElementType } from "@types";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! RESET WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? label: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? reset-search: {}
 */

const ResetInterface = (element: ConfigElementType) => {
  const ref = useRef<HTMLDivElement>(null);
  const { isLoading, isFetching, resetSearch } = searchService.useSearch();

  // Listeners
  useCustomEventListeners(ref, {
    "reset-search": resetSearch,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        label={element.settings.label}
      />
    </CustomElementWrapperTag>
  );
};

export default ResetInterface;
