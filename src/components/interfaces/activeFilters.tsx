import React, { useCallback, useRef } from "react";

import { searchService } from "@services";
import { type ConfigElementType } from "@types";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useCustomEventListeners,
  useWrapperAttributes,
} from "@utils";

/*
 ! ACTIVE FILTERS WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? label: string
 ? facet-key: string
 ? value: string

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? reset-search: {}
 ? reset-facets: {}
 ? reset-facet: {detail: { facetKey }}
 */

const ActiveFiltersInterface = (element: ConfigElementType) => {
  const ref = useRef<HTMLDivElement>(null);
  const {
    isLoading,
    isFetching,
    currentActiveFacets,
    resetSearch,
    resetFacets,
    resetFacetByKey,
  } = searchService.useSearch();

  // Handlers
  const onResetAll = useCallback(() => {
    resetSearch();
  }, [resetSearch]);

  const onResetFacets = useCallback(() => {
    resetFacets();
  }, [resetFacets]);

  const onResetFacet = useCallback(
    (e: Event) => {
      const { facetKey } = (e as CustomEvent).detail;
      resetFacetByKey(facetKey);
    },
    [resetFacetByKey]
  );

  // Listeners
  useCustomEventListeners(ref, {
    "reset-facets": onResetFacets,
    "reset-facet": onResetFacet,
    reset: onResetAll,
  });

  // Render
  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
    isActive: !!currentActiveFacets?.length,
  });

  return (
    <CustomElementWrapperTag
      ref={ref}
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      {currentActiveFacets?.map((facet) => (
        <CustomElementTag
          key={facet.key}
          data-element-type={getElementId(element)}
          label={facet.label}
          facet-key={facet.key}
          value={facet.active_values.join(", ")}
        />
      ))}
    </CustomElementWrapperTag>
  );
};

export default ActiveFiltersInterface;
