import React from "react";

import { searchService } from "@services";
import { type ConfigElementType } from "@types";
import {
  getElementId,
  getWrapperId,
  useCustomElementTags,
  useWrapperAttributes,
} from "@utils";

/*
 ! SUMMARY WIDGET INTERFACE

 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean

 * ATTRIBUTES PASSED:
 ? time-taken: number
 ? number-of-results: number
 */

const SummaryInterface = (element: ConfigElementType) => {
  const { timeTaken, nOfResults, isLoading, isFetching } =
    searchService.useSearch();

  const { CustomElementTag, CustomElementWrapperTag } =
    useCustomElementTags(element);
  const wrapperAttributes = useWrapperAttributes(element, {
    isLoading: isLoading || isFetching,
  });

  return (
    <CustomElementWrapperTag
      data-element-type={getWrapperId(element)}
      {...wrapperAttributes}
    >
      <CustomElementTag
        data-element-type={getElementId(element)}
        time-taken={timeTaken}
        number-of-results={nOfResults}
        loading={wrapperAttributes.loading}
      />
    </CustomElementWrapperTag>
  );
};

export default SummaryInterface;
