//! If this file is changed, please update the docs in the wiki

export interface SearchResponseType {
  search_results: SearchResultType[];
  search_results_count: number;
  search_results_page: number;
  search_results_pages: number;
  search_results_per_page: number;
  facets: SearchFacetsType[];
  took: number;
  max_score: number;
}

export interface SearchResultType {
  rendered_result: string;
  rendered_result_card?: string;
  rendered_source: string;
  url: string;
  nid: string;
  preview_image?: string;
  preview_text?: string;
  title: string;
  type: string;
  category?: string;
  id: string;
  score: number;
  created: string;
  excerpt: string | null;
}

export interface SearchFacetsType {
  active_values?: string[];
  label: string;
  key: string;
  count: number;
  min?: number;
  max?: number;
  results: ResultsType[];
}

export interface ResultsType {
  active?: boolean;
  label: string;
  count: number;
  key: string;
  children: ResultsType[];
}
