/* eslint-disable @typescript-eslint/no-explicit-any */
declare global {
  namespace JSX {
    // eslint-disable-next-line @typescript-eslint/consistent-indexed-object-style
    interface IntrinsicElements {
      [elemName: string]: any;
    }
  }
  interface Window {
    Drupal: any;
    drupalSettings: any;
  }
  const Drupal: {
    [key: string]: any;
    t: (key: string, args?: Record<string, string>) => string;
  };
}

export {};
