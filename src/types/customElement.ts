export interface CustomElementProps {
  id?: string;
  tag?: string;
  attributes?: Record<string, any>;
  children?: string;
  slots?: Record<string, any>;
}
