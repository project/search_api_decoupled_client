//! If this file is changed, please update the docs in the wiki

export type AutocompleteResponseType = AutocompleteSuggestionType[];

export interface AutocompleteSuggestionType {
  value: string;
  label: string;
}
