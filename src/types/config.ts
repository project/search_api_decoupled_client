//! If this file is changed, please update the docs in the wiki

import { type SearchResultType } from "./search";

export interface EndpointConfigType {
  id: string;
  label: string;
  host?: string; // this is only for development mock purposes
  base_url: string;
  base_url_autocomplete: string;
  state_manager?: "url" | "context";
  elements: Record<string, ConfigElementType>;
  layout: ConfigLayoutType;
}

export type ConfigType = Record<string, EndpointConfigType>;

export interface ConfigLayoutType {
  id: string;
  regions: Record<string, string[]>;
}

export interface WrapperSettingsType {
  label?: string;
  show_loading?: boolean;
  show_label?: boolean;
  orientation?: "horizontal" | "vertical";
  show_reset?: boolean;
  reset_text?: string;
}

export interface CustomElementSettingsType {
  custom_element?: string;
  custom_element_wrapper?: string;
}

export interface ConfigElementType<
  SettingsType =
    | InputSettingsType
    | ExposedFilterSettingsType
    | FacetSettingsType
    | SorterSettingsType
    | RenderedResultsSettingsType
    | CustomResultsSettingsType
    | ResultsSettingsType
> {
  id: string;
  type: ElementType;
  label?: string;
  weight?: string | number;
  widget?: WidgetType;
  behaviors?: {
    filter?: 0 | 1;
    url?: 0 | 1;
    collapsible?: 0 | 1;
    collapsed?: 0 | 1;
  };
  settings: SettingsType & WrapperSettingsType & CustomElementSettingsType;
}

export type WidgetType =
  // input
  | "default"
  | "simple"
  | "autocomplete"
  | "autocomplete-complex"
  // facets
  | "links"
  | "select"
  | "checkbox"
  // pagers
  | "full"
  | "mini"
  | "loadmore";

export type ElementType =
  | "search_input"
  | "active_filters"
  | "autocomplete"
  | "facet"
  | "sorting"
  | "exposed_filter"
  | "search_result"
  | "search_result_switch"
  | "search_result_custom"
  | "pager"
  | "loadmore"
  | "summary"
  | "refresh_results"
  | "reset_filters";

export type PagerType = "loadmore" | "pager";

export type OperatorSettingsType =
  | "between"
  | "equal"
  | "in"
  | "gt"
  | "lt"
  | "gte"
  | "lte"
  | "not_equal"
  | "not_in";

export interface ExposedFilterSettingsType {
  field_name?: string;
  field_type?: string;
  operator?: OperatorSettingsType;
  default_value?: string;
  default_value_2?: string;
}

export interface FacetSettingsType {
  facet?: string;
  filter?: {
    label?: string;
  };
  // counter
  show_numbers?: boolean;
  // reset
  hide_reset_when_no_selection?: boolean;
  // Hierarchy
  use_hierarchy?: boolean;
  expand_hierarchy?: boolean;
  // aliases
  field_alias?: string;
  url_alias?: string;
  // range
  min_type?: "fixed" | "search_result" | "stats";
  min_value?: number;
  max_type?: "fixed" | "search_result" | "stats";
  max_value?: number;
  step?: number;
}

export interface SorterSettingsType {
  default: string;
  info: Record<
    string,
    {
      sortable: boolean | number;
      asc: {
        enabled: boolean | number;
        label?: string;
      };
      desc: {
        enabled: boolean | number;
        label?: string;
      };
      default_sort_order: "asc" | "desc";
      weight: string;
    }
  >;
}

export interface InputSettingsType {
  no_empty?: boolean;
  placeholder?: string;
}

export interface ResultsSettingsType {
  columns?: number | string;
  no_results: string;
  show_switch?: boolean;
  loading: string;
}
export interface RenderedResultsSettingsType extends ResultsSettingsType {
  field: keyof SearchResultType;
  field_grid: keyof SearchResultType;
}

export interface CustomResultsSettingsType extends ResultsSettingsType {
  field_category: keyof SearchResultType;
  field_title: keyof SearchResultType;
  field_text: keyof SearchResultType;
  field_image: keyof SearchResultType;
  field_date: keyof SearchResultType;
}
