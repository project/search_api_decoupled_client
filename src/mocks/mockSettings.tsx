import React, { useContext, useMemo, useState } from "react";
import { createPortal } from "react-dom";

import {
  MockConfigContext,
  MockedInputs,
  MockedLayouts,
  MockedPagers,
  MockedResults,
} from "./mockProvider";

const MockSettingsUI = () => {
  const {
    setSelectedMockedLayout,
    setSelectedMockedPager,
    setSelectedMockedResults,
    setSelectedMockedInput,
    selectedMockedInput,
    selectedMockedLayout,
    selectedMockedPager,
    selectedMockedResults,
    selectedDemoInstance,
    setSelectedDemoInstance,
    selectedEndpointName,
    setSelectedEndpointName,
    selectedStateManager,
    setSelectedStateManager,
  } = useContext(MockConfigContext);

  const baseUrlSettings = useMemo(
    () => [
      {
        label: "Search API host",
        selectedValue: selectedDemoInstance,
        setSelectedValue: setSelectedDemoInstance,
        placeholder: "search.1xinternet.de",
      },
      {
        label: "Search API endpoint name",
        selectedValue: selectedEndpointName,
        setSelectedValue: setSelectedEndpointName,
        placeholder: "default",
      },
    ],
    [
      selectedDemoInstance,
      setSelectedDemoInstance,
      selectedEndpointName,
      setSelectedEndpointName,
    ]
  );

  const stateManagerSettings = useMemo(
    () => ({
      label: "State manager",
      options: ["url", "context"],
      selectedValue: selectedStateManager,
      setSelectedValue: setSelectedStateManager,
    }),
    [selectedStateManager, setSelectedStateManager]
  );

  const uiSettings = [
    useMemo(
      () => ({
        label: "Layout",
        options: MockedLayouts,
        disabledOptions: ["rendered-switch"],
        selectedValue: selectedMockedLayout,
        setSelectedValue: setSelectedMockedLayout,
      }),
      [selectedMockedLayout, setSelectedMockedLayout]
    ),
    useMemo(
      () => ({
        label: "Input",
        options: MockedInputs,
        disabledOptions: ["autocomplete"],
        selectedValue: selectedMockedInput,
        setSelectedValue: setSelectedMockedInput,
      }),
      [selectedMockedInput, setSelectedMockedInput]
    ),
    useMemo(
      () => ({
        label: "Results",
        options: MockedResults,
        disabledOptions: ["autocomplete"],
        selectedValue: selectedMockedResults,
        setSelectedValue: setSelectedMockedResults,
      }),
      [selectedMockedResults, setSelectedMockedResults]
    ),
    useMemo(
      () => ({
        label: "Pager",
        options: MockedPagers,
        disabledOptions: ["load-more", "mini"],
        selectedValue: selectedMockedPager,
        setSelectedValue: setSelectedMockedPager,
      }),
      [selectedMockedPager, setSelectedMockedPager]
    ),
  ];

  const [modalOpen, setModalOpen] = useState(!selectedDemoInstance);

  return (
    <>
      <button
        className="dev"
        onClick={() => {
          setModalOpen(true);
        }}
      >
        settings
      </button>
      {modalOpen && (
        <div
          className="modal"
          onClick={() => {
            setModalOpen(!selectedDemoInstance || false);
          }}
        >
          <div
            className="container"
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <div className="title">DEVELOPMENT SETTINGS</div>

            <div className="full-row">
              <div className="first-section">
                {baseUrlSettings.map((setting) => (
                  <div key={setting.label}>
                    <label>{setting.label}</label>
                    <input
                      placeholder={setting.placeholder}
                      defaultValue={setting.selectedValue}
                      autoFocus={
                        !selectedDemoInstance &&
                        setting.label === "Search API host"
                      }
                      onChange={(e) => {
                        setting.setSelectedValue(e.target.value);
                      }}
                    ></input>
                  </div>
                ))}
              </div>

              <p className="hint">
                Your host should be running Search API Decoupled module, have a
                default endpoint configured and CORS enabled.
              </p>
            </div>

            <div className="full-row">
              <div>
                <label>{stateManagerSettings.label}</label>
                <select
                  onChange={(e) => {
                    stateManagerSettings.setSelectedValue(
                      e.target.value as never
                    );
                  }}
                >
                  {stateManagerSettings.options.map((option) => (
                    <option
                      key={option}
                      selected={option === stateManagerSettings.selectedValue}
                    >
                      {option}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            {uiSettings.map((setting, index) => (
              <div key={setting.label}>
                <label>{setting.label}</label>
                <select
                  disabled={selectedMockedLayout === "decoupled" && index !== 0}
                  onChange={(e) => {
                    setting.setSelectedValue(e.target.value as never);
                  }}
                >
                  {setting.options.map((option) => (
                    <option
                      key={option}
                      selected={option === setting.selectedValue}
                      disabled={setting.disabledOptions?.includes(option)}
                    >
                      {option}
                    </option>
                  ))}
                </select>
              </div>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

const MockSettingsPortal = () => {
  const errorEl = <> Couldn&apos;t find the entrypoint for mock settings </>;

  try {
    const portalEl = document.querySelector("#mock-settings");

    if (portalEl) {
      return createPortal(<MockSettingsUI />, portalEl);
    } else {
      return errorEl;
    }
  } catch (e) {
    return errorEl;
  }
};

export default MockSettingsPortal;
