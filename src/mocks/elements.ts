import { type ConfigElementType } from "@types";

const mockedElements: Record<string, ConfigElementType> = {
  // INPUTS
  "input-default": {
    id: "input-default",
    type: "search_input",
    weight: "-10",
    behaviors: {
      filter: 0,
      url: 0,
    },
    widget: "default",
    settings: {
      placeholder: "Search for contents",
      filter: {
        label: "Your search",
      },
    },
  },
  "input-autocomplete": {
    id: "input-autocomplete",
    type: "autocomplete",
    weight: "-10",
    behaviors: {
      filter: 0,
      url: 0,
    },
    widget: "autocomplete",
    settings: {
      placeholder: "Search for contents",
      filter: {
        label: "Your search",
      },
    },
  },

  // SUMMARY
  summary: {
    id: "summary",
    type: "summary",
    weight: "-8",
    settings: {},
  },

  // refresh and clear filters
  refresh: {
    id: "refresh",
    type: "refresh_results",
    weight: "4",
    settings: {
      label: "Refresh",
      custom_element: "qz-search-refresh",
      show_loading: false,
    },
  },
  reset: {
    id: "reset",
    type: "reset_filters",
    weight: "3",
    settings: {
      label: "Reset",
      custom_element: "qz-search-reset",
      show_loading: false,
    },
  },

  // sorting
  "sorting-element": {
    id: "sorting-element",
    type: "sorting",
    weight: 9,
    settings: {
      show_loading: false,
      default: "field_relevance",
      label: "Sort by",
      show_label: true,
      info: {
        field_relevance: {
          sortable: true,
          asc: {
            enabled: true,
            label: "Relevance asc",
          },
          desc: {
            enabled: true,
            label: "Relevance desc",
          },
          default_sort_order: "asc",
          weight: "-9",
        },
        field_rating: {
          sortable: true,
          asc: {
            enabled: false,
          },
          desc: {
            enabled: true,
            label: "Rating desc",
          },
          default_sort_order: "desc",
          weight: "-10",
        },
      },
    },
  },

  // EXPOSED FILTERS
  "exposed-filter": {
    id: "exposed-filter",
    type: "exposed_filter",
    weight: 3,
    settings: {
      show_label: false,
      reset_text: "Reset",
      show_reset: false,
      show_loading: false,
      orientation: "vertical",
      field_name: "field_destinations_computed",
      operator: "in",
      default_value: "8171",
      default_value_2: "",
      custom_element_wrapper: "div",
      field_type: "integer",
    },
  },

  // FACETS
  "content-type-checkbox-vertical": {
    id: "content-type-checkbox-vertical",
    type: "facet",
    weight: "-9",
    label: "Content type",
    behaviors: {
      collapsible: 0,
      collapsed: 0,
    },
    settings: {
      facet: "content_type",
      orientation: "vertical",
      show_numbers: false,
      show_label: true,
      show_reset: true,
      reset_text: "reset",
      hide_reset_when_no_selection: false,
      use_hierarchy: false,
      expand_hierarchy: false,
      field_alias: "type",
      url_alias: "type",
    },
    widget: "checkbox",
  },

  "content-type-links-horizontal": {
    id: "content-type-links-horizontal",
    type: "facet",
    label: "Content type",
    weight: "-9",
    behaviors: {
      collapsible: 0,
      collapsed: 0,
    },
    settings: {
      facet: "content_type",
      orientation: "horizontal",
      show_numbers: true,
      show_reset: true,
      show_label: false,
      reset_text: "reset",
      hide_reset_when_no_selection: false,
      use_hierarchy: false,
      expand_hierarchy: false,
      field_alias: "type",
      url_alias: "type",
      custom_element: "qz-link-facet",
    },
    widget: "links",
  },
  "content-type-links-vertical": {
    id: "content-type-links-vertical",
    type: "facet",
    weight: "-9",
    label: "Content type",
    behaviors: {
      collapsible: 0,
      collapsed: 0,
    },
    settings: {
      facet: "content_type",
      orientation: "vertical",
      show_numbers: true,
      show_reset: true,
      show_label: true,
      reset_text: "reset",
      hide_reset_when_no_selection: false,
      use_hierarchy: false,
      expand_hierarchy: false,
      field_alias: "type",
      url_alias: "type",
      custom_element: "qz-link-facet",
    },
    widget: "links",
  },
  "category-checkbox-horizontal": {
    id: "category-checkbox-horizontal",
    type: "facet",
    weight: "-7",
    label: "Categories",
    behaviors: {
      collapsible: 0,
      collapsed: 0,
    },
    settings: {
      facet: "category",
      orientation: "horizontal",
      show_numbers: true,
      show_label: false,
      show_reset: true,
      reset_text: "reset",
      hide_reset_when_no_selection: false,

      use_hierarchy: false,
      expand_hierarchy: false,
      field_alias: "field_category",
      url_alias: "category",
    },
    widget: "checkbox",
  },
  "category-checkbox-vertical": {
    id: "category-checkbox-vertical",
    type: "facet",
    weight: "-7",
    label: "Categories",
    behaviors: {
      collapsible: 0,
      collapsed: 0,
    },
    settings: {
      facet: "category",
      orientation: "vertical",
      show_numbers: true,
      show_reset: true,
      show_label: true,
      reset_text: "reset",
      hide_reset_when_no_selection: false,
      use_hierarchy: false,
      expand_hierarchy: false,
      field_alias: "field_category",
      url_alias: "category",
    },
    widget: "checkbox",
  },
  "date-links-vertical": {
    id: "date-links-vertical",
    type: "facet",
    weight: "-6",
    behaviors: {
      collapsible: 1,
      collapsed: 1,
    },
    settings: {
      orientation: "vertical",
      facet: "date",
      show_numbers: false,
      show_label: true,
      show_reset: true,
      reset_text: "reset",
      hide_reset_when_no_selection: false,
      use_hierarchy: true,
      expand_hierarchy: false,
      field_alias: "field_date",
      custom_element: "qz-link-facet",
    },
    label: "Date",
    widget: "links",
  },

  // RESULTS
  "result-rendered": {
    id: "result-rendered",
    type: "search_result",
    weight: "-5",
    settings: {
      field: "rendered_result",
      no_results: "No results found.",
      show_loading: true,
      loading: "Loading...",
    },
  },
  "result-custom": {
    id: "result-custom",
    type: "search_result_custom",
    weight: "-5",
    settings: {
      no_results: "No results found.",
      show_loading: true,
      loading: "Loading...",
      columns: "4",
      field_category: "type",
      field_title: "title",
      field_text: "preview_text",
      field_image: "preview_image",
      field_date: "created",
    },
  },
  "result-rendered-switch": {
    id: "result-rendered-switch",
    type: "search_result_switch",
    weight: "-5",
    settings: {
      field: "rendered_result",
      field_grid: "rendered_result_card",
      columns: 3,
      no_results: "No results found.",
      loading: "Loading...",
      show_loading: true,
    },
  },

  // PAGERS
  "pager-load-more": {
    id: "pager-load-more",
    type: "loadmore",
    weight: "-4",
    widget: "loadmore",
    settings: {
      custom_element: "qz-load-more",
      custom_element_wrapper: "div",
    },
  },
  "pager-full": {
    id: "pager-full",
    type: "pager",
    weight: "-4",
    widget: "full",
    settings: {},
  },
  "pager-mini": {
    id: "pager-mini",
    type: "pager",
    weight: "-4",
    widget: "mini",
    settings: {
      custom_element: "qz-mini-pager",
    },
  },
};

export default mockedElements;
