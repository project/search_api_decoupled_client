import mockedElements from "./elements";
import { getMockedLayout } from "./layouts";
import {
  type MockedResultsType,
  type MockedInputType,
  type MockedLayoutType,
  type MockedPagerType,
} from "./mockProvider";

const updateMockedConfig = (props: {
  selectedMockedInput: MockedInputType;
  selectedMockedResults: MockedResultsType;
  selectedMockedLayout: MockedLayoutType;
  selectedMockedPager: MockedPagerType;
  selectedDemoInstance: string;
  selectedEndpointName: string;
  selectedStateManager: "url" | "context";
}) => {
  const {
    selectedMockedInput,
    selectedMockedResults,
    selectedMockedLayout,
    selectedMockedPager,
    selectedDemoInstance,
    selectedEndpointName,
    selectedStateManager,
  } = props;

  if (selectedMockedLayout === "decoupled") {
    window.drupalSettings = {
      search_api_endpoint: {
        [selectedEndpointName]: {
          id: "default",
          host: "https://" + selectedDemoInstance,
        },
      },
    };
    return;
  }

  switch (selectedMockedResults) {
    case "rendered-switch":
    case "rendered":
      window.drupalSettings = {
        search_api_endpoint: {
          "default-rendered-switch": {
            id: selectedEndpointName,
            label: "Default Rendered Results",
            state_manager: selectedStateManager,
            host: "https://" + selectedDemoInstance,
            base_url: "/api/search/" + selectedEndpointName,
            base_url_autocomplete:
              "/search_api_autocomplete/" + selectedEndpointName,
            elements: mockedElements,
            layout: getMockedLayout(
              selectedMockedLayout,
              selectedMockedInput,
              selectedMockedResults,
              selectedMockedPager
            ),
          },
        },
      };
      break;
    case "custom":
    default:
      window.drupalSettings = {
        search_api_endpoint: {
          "default-local-switch": {
            id: "default",
            label: "Default Local Results",
            state_manager: selectedStateManager,
            host: "https://" + selectedDemoInstance,
            base_url: "/api/search/" + selectedEndpointName,
            base_url_autocomplete:
              "/search_api_autocomplete/" + selectedEndpointName,
            elements: mockedElements,
            layout: getMockedLayout(
              selectedMockedLayout,
              selectedMockedInput,
              selectedMockedResults,
              selectedMockedPager
            ),
          },
        },
      };
  }
};

export default updateMockedConfig;
