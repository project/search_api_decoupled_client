import React, {
  type ReactNode,
  createContext,
  useState,
  useEffect,
} from "react";
import updateMockedConfig from "./updateMockedConfig";

export const MockedLayouts = ["sidebar", "simple", "three-column", "decoupled"];
export type MockedLayoutType =
  | "sidebar"
  | "simple"
  | "three-column"
  | "decoupled";

export const MockedResults = ["rendered", "rendered-switch", "custom"];
export type MockedResultsType = "rendered" | "rendered-switch" | "custom";

export const MockedPagers = ["full", "mini", "load-more"];
export type MockedPagerType = "full" | "mini" | "load-more";

export const MockedInputs = ["default", "autocomplete"];
export type MockedInputType = "default" | "autocomplete";

const MockConfigContext = createContext({
  selectedMockedLayout: "simple" as MockedLayoutType,
  setSelectedMockedLayout: (selectedMockedLayout: MockedLayoutType) => {
    console.log(selectedMockedLayout);
  },
  selectedMockedInput: "default" as MockedInputType,
  setSelectedMockedInput: (selectedMockedInput: MockedInputType) => {
    console.log(selectedMockedInput);
  },
  selectedMockedResults: "custom" as MockedResultsType,
  setSelectedMockedResults: (selectedMockedResults: MockedResultsType) => {
    console.log(selectedMockedResults);
  },
  selectedMockedPager: "full" as MockedPagerType,
  setSelectedMockedPager: (selectedMockedPager: MockedPagerType) => {
    console.log(selectedMockedPager);
  },
  selectedDemoInstance: "",
  setSelectedDemoInstance: (selectedDemoInstance: string) => {
    console.log(selectedDemoInstance);
  },
  selectedEndpointName: "default",
  setSelectedEndpointName: (selectedEndpointName: string) => {
    console.log(selectedEndpointName);
  },
  selectedStateManager: "url" as "url" | "context",
  setSelectedStateManager: (selectedStateManager: "url" | "context") => {
    console.log(selectedStateManager);
  },
});

const MockConfigContextProvider = (props: { children: ReactNode }) => {
  // Selected options
  const [selectedMockedLayout, setSelectedMockedLayout] =
    useState<MockedLayoutType>("simple");
  const [selectedMockedInput, setSelectedMockedInput] =
    useState<MockedInputType>("default");
  const [selectedMockedResults, setSelectedMockedResults] =
    useState<MockedResultsType>("custom");
  const [selectedMockedPager, setSelectedMockedPager] =
    useState<MockedPagerType>("full");
  const [selectedDemoInstance, setSelectedDemoInstance] = useState<string>(
    localStorage.getItem("selectedDemoInstance") ?? ""
  );
  const [selectedEndpointName, setSelectedEndpointName] = useState<string>(
    localStorage.getItem("selectedEndpointName") ?? "default"
  );
  const [selectedStateManager, setSelectedStateManager] = useState<
    "url" | "context"
  >("url");

  useEffect(() => {
    localStorage.setItem("selectedDemoInstance", selectedDemoInstance);
  }, [selectedDemoInstance]);

  useEffect(() => {
    localStorage.setItem("selectedEndpointName", selectedEndpointName);
  }, [selectedEndpointName]);

  // Update the mocked config
  useEffect(() => {
    updateMockedConfig({
      selectedMockedLayout,
      selectedMockedInput,
      selectedMockedResults,
      selectedMockedPager,
      selectedDemoInstance,
      selectedEndpointName,
      selectedStateManager,
    });
  }, [
    selectedMockedLayout,
    selectedMockedInput,
    selectedMockedResults,
    selectedMockedPager,
    selectedDemoInstance,
    selectedEndpointName,
    selectedStateManager,
  ]);

  return (
    <MockConfigContext.Provider
      value={{
        selectedMockedLayout,
        setSelectedMockedLayout,
        selectedMockedInput,
        setSelectedMockedInput,
        selectedMockedResults,
        setSelectedMockedResults,
        selectedMockedPager,
        setSelectedMockedPager,
        selectedDemoInstance,
        setSelectedDemoInstance,
        selectedEndpointName,
        setSelectedEndpointName,
        selectedStateManager,
        setSelectedStateManager,
      }}
    >
      {props.children}
    </MockConfigContext.Provider>
  );
};

export { MockConfigContext, MockConfigContextProvider };
