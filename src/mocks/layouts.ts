import {
  type MockedResultsType,
  type MockedLayoutType,
  type MockedPagerType,
  type MockedInputType,
} from "./mockProvider";

const simpleLayout = (
  input: MockedInputType,
  results: MockedResultsType,
  pager: MockedPagerType
) => ({
  id: "layoutSimple",
  regions: {
    top: [
      "input-" + input,
      "category-checkbox-horizontal",
      "summary",
      "range",
      "result-" + results,
    ],
    content: [],
    bottom: ["pager-" + pager],
  },
});

const sidebarLayout = (
  input: MockedInputType,
  results: MockedResultsType,
  pager: MockedPagerType
) => ({
  id: "layoutSidebar",
  regions: {
    aside: ["category-checkbox-vertical", "content-type-checkbox-vertical"],
    top: ["input-" + input, "summary"],
    content: ["result-" + results],
    bottom: ["pager-" + pager],
  },
});

const threeColumnLayout = (
  input: MockedInputType,
  results: MockedResultsType,
  pager: MockedPagerType
) => ({
  id: "layoutThreeColumn",
  regions: {
    aside: ["category-checkbox-vertical"],
    top: ["input-" + input, "summary"],
    content: ["result-" + results],
    "aside-right": ["content-type-checkbox-vertical"],
    bottom: ["pager-" + pager],
  },
});

const getMockedLayout = (
  layout: MockedLayoutType,
  input: MockedInputType,
  results: MockedResultsType,
  pager: MockedPagerType
) => {
  const layoutMap = {
    simple: simpleLayout,
    sidebar: sidebarLayout,
    "three-column": threeColumnLayout,
    decoupled: simpleLayout,
  };

  const selectedLayout = layoutMap[layout] ?? simpleLayout;

  return selectedLayout(input, results, pager);
};

export { getMockedLayout };
