const encodeText = (text: string): string => {
  return text.replace(/[\u00A0-\u9999<>&]/gim, function (i) {
    return "&#" + i.charCodeAt(0).toString() + ";";
  });
};

export default encodeText;
