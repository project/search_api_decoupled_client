/* eslint-disable @typescript-eslint/no-explicit-any */
/*
The code here is borrowed from the xi-search-client project:
https://git.1xinternet.de/1x-contrib-modules/xi-search-client/-/tree/develop/src/lib
 */

type _translateType = (
  str: string,
  args?: Record<string, any>,
  options?: Record<string, any>
) => string;
type _pluralType = (
  count: number,
  singular: string,
  plural: string,
  args: Record<string, any>,
  options: Record<string, any>
) => string;
type _formatStringType = (str: string, args: Record<string, any>) => string;
type _stringReplaceType = (
  str: string,
  args: Record<string, any>,
  keys: string[] | null
) => string;

const DELIMITER = String.fromCharCode(3);

// This is an internal Drupal function and the complexity of it can be ignored
// eslint-disable-next-line complexity
const _stringReplace: _stringReplaceType = (str, args, keys) => {
  if (!str || str.length === 0) {
    return str;
  }

  // If the array of keys is not passed then collect the keys from the args.
  if (!Array.isArray(keys)) {
    keys = Object.keys(args || {});

    // Order the keys by the character length. The shortest one is the first.
    keys.sort((a, b) => a.length - b.length);
  }

  if (keys.length === 0) {
    return str;
  }

  // Take next longest one from the end.
  const key = keys.pop();
  if (!str || !key) {
    return "";
  }
  const fragments = str.split(key);

  if (keys.length) {
    for (let i = 0; i < fragments.length; i++) {
      // Process each fragment with a copy of remaining keys.
      fragments[i] = _stringReplace(fragments[i], args, keys.slice(0));
    }
  }

  return fragments.join(args[key]);
};

const _formatString: _formatStringType = (str, args) => {
  // Keep args intact.
  const processedArgs: Record<string, any> = {};
  // Transform arguments before inserting them.
  Object.keys(args || {}).forEach((key) => {
    switch (key.charAt(0)) {
      // Escaped only.
      case "@":
      case "!":
      default:
        processedArgs[key] = args[key];
        break;
    }
  });
  return _stringReplace(str, processedArgs, null);
};

const _translate: _translateType = (str, args, options) => {
  options = options ?? {};
  options.context = options.context || "";
  if (args) {
    str = _formatString(str, args);
  }
  return str;
};

const _plural: _pluralType = (
  count,
  singular,
  plural,
  args = {},
  options = {}
) => {
  if (typeof count === "undefined") {
    count = 0;
  }
  args = args || {};
  args["@count"] = count;
  const pluralDelimiter = DELIMITER;
  const translations = _translate(
    singular + pluralDelimiter + plural,
    args,
    options
  ).split(pluralDelimiter);
  let index = 0;
  if (args["@count"] !== 1) {
    index = 1;
  }
  return translations[index];
};

if (typeof window.Drupal === "undefined") {
  window.Drupal = {
    t: _translate,
    formatPlural: _plural,
    formatString: _formatString,
  };
}

export default {};
