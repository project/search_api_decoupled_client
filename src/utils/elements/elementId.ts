import { type ConfigElementType } from "@types";

const getElementId = (element: ConfigElementType): string | undefined => {
  if (element.settings.custom_element) return undefined;
  else return element.type + (element.widget ? "-" + element.widget : "");
};

export default getElementId;
