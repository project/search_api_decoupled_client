/* eslint-disable @typescript-eslint/no-explicit-any */
import { useMemo } from "react";
import { type ConfigElementType } from "@types";

const useCustomElementTags = (
  element?: ConfigElementType,
  childTag?: string
): any => {
  const CustomElementTag: any = useMemo(
    () => element?.settings.custom_element ?? "div",
    [element]
  );
  const CustomElementWrapperTag: any = useMemo(
    () => element?.settings.custom_element_wrapper ?? "div",
    [element]
  );
  const CustomElementChildTag: any = useMemo(
    () =>
      childTag
        ? (element?.settings.custom_element ?? "div") + "-" + childTag
        : undefined,
    [element, childTag]
  );
  return { CustomElementTag, CustomElementWrapperTag, CustomElementChildTag };
};

export default useCustomElementTags;
