import { type RefObject, useEffect } from "react";

const useCustomEventListeners = (
  ref: RefObject<HTMLDivElement>,
  events: Record<string, EventListener>
) => {
  useEffect(() => {
    const { current } = ref;
    Object.entries(events).forEach(([event, callback]) => {
      current?.addEventListener(event, callback);
    });
    return () => {
      Object.entries(events).forEach(([event, callback]) => {
        current?.removeEventListener(event, callback);
      });
    };
  }, [events, ref]);
};

export default useCustomEventListeners;
