import { type ConfigElementType } from "@types";

const getWrapperId = (element: ConfigElementType): string | undefined => {
  if (element.settings.custom_element_wrapper) return undefined;
  else {
    return (
      "wrapper-" + element.type + (element.widget ? "-" + element.widget : "")
    );
  }
};

export default getWrapperId;
