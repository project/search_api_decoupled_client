import { type ConfigElementType } from "@types";

const getChildId = (element: ConfigElementType): string | undefined => {
  if (element.settings.custom_element) return undefined;
  else {
    return (
      element.type + (element.widget ? "-" + element.widget : "") + "-child"
    );
  }
};

export default getChildId;
