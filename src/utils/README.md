### *src/utils*
## Utilities folder
We use this folder to organise all utility functions.

## Logic behind this folder structure
This structure has been inspired by Atomic Design methodology. You can read more about it [here](https://bradfrost.com/blog/post/atomic-web-design/).
