import {
  type ConfigElementType,
  type FacetSettingsType,
  type SearchFacetsType,
} from "@types";
import trueOrUndefined from "../trueOrUndefined";

/*
 * ATTRIBUTES PASSED:
 ? lower: number
 ? higher: number
 ? step: number
 ? single: boolean
 ? lower-label: string
 ? higher-label: string
 */

const useRangeAttributes = (
  facet: ConfigElementType<FacetSettingsType>,
  facetState?: SearchFacetsType
) => {
  return {
    lower: calculateMin(facet, facetState),
    higher: calculateMax(facet, facetState),
    step: facet.settings.step,
    single: trueOrUndefined(
      !!facet.settings.min_value && !facet.settings.max_value
    ),
    "lower-label": `${Drupal.t("Min")} ${
      facet.settings.label?.toLocaleLowerCase() ?? ""
    }`,
    "higher-label": `${Drupal.t("Max")} ${
      facet.settings.label?.toLocaleLowerCase() ?? ""
    }`,
  };
};

const calculateMin = (
  facet: ConfigElementType<FacetSettingsType>,
  facetState?: SearchFacetsType
) => {
  let min;
  switch (facet.settings.min_type) {
    case "search_result":
      if (!facetState?.results.length) {
        min = undefined;
      } else {
        min = Math.min(
          ...facetState.results.map((result) => Number(result.key))
        );
      }
      break;
    case "stats":
      min = facetState?.min;
      break;
    case "fixed":
    default:
      min = facet.settings.min_value;
  }
  return min;
};

const calculateMax = (
  facet: ConfigElementType<FacetSettingsType>,
  facetState?: SearchFacetsType
) => {
  let max;
  switch (facet.settings.max_type) {
    case "search_result":
      if (!facetState?.results.length) {
        max = undefined;
      } else {
        max = Math.max(
          ...facetState?.results.map((result) => Number(result.key))
        );
      }
      break;
    case "stats":
      max = facetState?.max;
      break;
    case "fixed":
    default:
      max = facet.settings.max_value;
  }
  return max;
};

export default useRangeAttributes;
