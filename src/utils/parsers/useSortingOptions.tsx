import { type ConfigElementType, type SorterSettingsType } from "@types";

const useSortingOptions = (element: ConfigElementType<SorterSettingsType>) =>
  Object.entries(element.settings.info).flatMap(([key, value]) => {
    const options = [];
    if (value.asc.enabled) {
      options.push({
        key,
        order: "asc",
        label: value.asc.label,
      });
    }
    if (value.desc.enabled) {
      options.push({
        key,
        order: "desc",
        label: value.desc.label,
      });
    }
    return options;
  }, []);

export default useSortingOptions;
