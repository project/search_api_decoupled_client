import { type ConfigElementType } from "@types";
import { trueOrUndefined } from "@utils";

/*
 * WRAPPER ATTRIBUTES PASSED:
 ? orientation: string
 ? loading: boolean
 ? resettable: boolean
 ? reset-text: string
 ? label: string
 ? show-label: boolean
  */

const useWrapperAttributes = (
  element: ConfigElementType,
  status: { isLoading: boolean; isActive?: boolean }
) => {
  return {
    // state
    loading: trueOrUndefined(element.settings.show_loading && status.isLoading),
    // layout
    orientation: element.settings.orientation,
    // reset
    "reset-text": element.settings.reset_text,
    resettable: trueOrUndefined(element.settings.show_reset && status.isActive),
    // label
    label: element.settings.label,
    "show-label": trueOrUndefined(element.settings.show_label),
  };
};

export default useWrapperAttributes;
