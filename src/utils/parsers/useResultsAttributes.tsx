import { type ResultsSettingsType, type ConfigElementType } from "@types";
import { trueOrUndefined } from "@utils";

/*
 * WRAPPER ATTRIBUTES PASSED:
 ? no-results-text: string
 ? loading-text: string
 ? columns: number
 ? switchable: boolean (true)
 ? mode: "list" | "grid"
*/

const useResultsAttributes = (
  element: ConfigElementType<ResultsSettingsType>,
  status?: { mode?: "list" | "grid" }
) => {
  return {
    // state
    "no-results-text": element.settings.no_results,
    "loading-text": element.settings.loading,
    // layout
    columns: element.settings.columns,
    switchable: trueOrUndefined(element.settings.show_switch),
    mode: status?.mode,
  };
};

export default useResultsAttributes;
