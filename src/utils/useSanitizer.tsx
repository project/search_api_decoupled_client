import DOMPurify from "dompurify";

const useSanitizer = () => {
  return DOMPurify.sanitize;
};

export default useSanitizer;
