const trueOrUndefined = (value: boolean | undefined): string | undefined => {
  return value ? "true" : undefined;
};

export default trueOrUndefined;
