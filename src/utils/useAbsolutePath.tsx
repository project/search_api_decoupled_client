import { apiService } from "@services";
import { useCallback } from "react";

const useAbsolutePath = () => {
  const { host } = apiService.useBaseUrl();

  const getAbsolutePath = useCallback(
    (url?: string) => {
      if (!url || url === "" || typeof url !== "string") return "";
      else if (url.startsWith("http")) return url;
      else return `${host}${url}`;
    },
    [host]
  );

  return getAbsolutePath;
};

export default useAbsolutePath;
