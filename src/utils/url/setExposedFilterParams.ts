const setExposedFilterParams = (
  searchParams: URLSearchParams,
  filterName: string,
  values: {
    from?: string;
    to?: string;
    value?: string;
    operator?: string;
  }
) => {
  if (values.value) {
    // Single value
    searchParams.set(`${filterName}`, values.value);
    searchParams.delete(`${filterName}[min]`);
    searchParams.delete(`${filterName}[max]`);
    if (
      values?.operator &&
      !["in", "equal", "between"].includes(values.operator)
    ) {
      searchParams.set(`operator[${filterName}]`, values.operator);
    } else searchParams.delete(`operator[${filterName}]`);
  } else {
    searchParams.delete(`${filterName}`);
    searchParams.delete(`operator[${filterName}]`);
    // Range values
    if (values.from === undefined) searchParams.delete(`${filterName}[min]`);
    else searchParams.set(`${filterName}[min]`, values.from);
    if (values.to === undefined) searchParams.delete(`${filterName}[max]`);
    else searchParams.set(`${filterName}[max]`, values.to);
  }
};

export default setExposedFilterParams;
