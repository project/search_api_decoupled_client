import { useContext } from "react";

import { AppConfigContext } from "./configProvider";

const useAppKey = () => {
  const { appKey } = useContext(AppConfigContext);
  return appKey;
};

export default useAppKey;
