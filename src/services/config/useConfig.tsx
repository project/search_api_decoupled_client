import { useCallback, useEffect, useMemo, useState } from "react";
import {
  type PagerType,
  type ConfigType,
  type InputSettingsType,
} from "@types";
import { apiService } from "@services";

import useAppKey from "./useAppKey";

const useConfig = () => {
  // Request drupalSettings
  const config: ConfigType = useMemo(() => {
    return window?.drupalSettings?.search_api_endpoint ?? {};
  }, [window]);

  // App configurations available
  const appKeys = Object.keys(config).filter((key) => !!key);
  const appIds = appKeys.map((key) => config[key].id);

  const appPagerTypes = useMemo(() => {
    return appKeys.map((key) => {
      const pagerElement = Object.entries(config[key].elements).find(
        (e) =>
          (e[1].type === "pager" || e[1].type === "loadmore") &&
          Object.values(config[key].layout.regions).find((r) =>
            r.includes(e[0])
          )
      );
      return (pagerElement?.[1].type ?? "loadmore") as PagerType;
    });
  }, [config, appKeys]);

  const appAvoidEmptyQueryConfig = useMemo(() => {
    return appKeys.map((key) => {
      // find search_input or autocomplete element for the app and check if it has no_empty
      const inputElement = Object.entries(config[key].elements).find(
        (e) =>
          (e[1].type === "search_input" || e[1].type === "autocomplete") &&
          Object.values(config[key].layout.regions).find((r) =>
            r.includes(e[0])
          )
      );
      return (inputElement?.[1].settings as InputSettingsType)?.no_empty;
    });
  }, [config, appKeys]);

  // App specific configurations
  const appKey = useAppKey();
  const [appConfig, setAppConfig] = useState(config[appKey]);
  const stateManager = useMemo(() => appConfig?.state_manager, [appConfig]);

  const { data: fetchedConfig } = apiService.useConfigApi(
    appConfig?.host,
    appConfig?.id,
    Object.keys(appConfig ?? {}).length > 2
  );
  useEffect(() => {
    if (fetchedConfig) {
      setAppConfig({ host: config[appKey].host, ...fetchedConfig });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fetchedConfig]);

  const appPortalKeys = useMemo(
    () => (appKey ? Object.keys(appConfig?.layout?.regions ?? {}) : undefined),
    [appConfig, appKey]
  );
  const appElements = useMemo(
    () => (appKey ? Object.values(appConfig?.elements ?? {}) : undefined),
    [appConfig, appKey]
  );

  const appElementsByPortal = useCallback(
    (portalKey: string) =>
      appConfig?.layout?.regions?.[portalKey]?.map((elementKey: string) =>
        appElements?.find((element) => element.id === elementKey)
      ),
    [appConfig, appElements]
  );

  return {
    // APPS
    appKeys,
    appIds,
    appPagerTypes,
    appAvoidEmptyQueryConfig,
    // Specific app
    stateManager,
    appConfig,
    appPortalKeys,
    appElements,
    appElementsByPortal,
  };
};

export default useConfig;
