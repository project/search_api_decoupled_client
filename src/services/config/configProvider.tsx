import React, { type ReactNode, createContext } from "react";

import { type PagerType } from "@types";

const AppConfigContext = createContext({
  appKey: "" as string,
  appId: "" as string,
  pagerType: "loadmore" as PagerType,
  avoidEmptyQuery: false,
});

const AppConfigContextProvider = (props: {
  children: ReactNode;
  appKey: string;
  appId: string;
  pagerType: PagerType;
  avoidEmptyQuery?: boolean;
}) => {
  return (
    <AppConfigContext.Provider
      value={{
        appKey: props.appKey,
        appId: props.appId,
        pagerType: props.pagerType,
        avoidEmptyQuery: props.avoidEmptyQuery ?? false,
      }}
    >
      {props.children}
    </AppConfigContext.Provider>
  );
};

export { AppConfigContext, AppConfigContextProvider };
