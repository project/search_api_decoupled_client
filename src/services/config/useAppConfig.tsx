import { useContext } from "react";

import { AppConfigContext } from "./configProvider";

const useAppConfig = () => {
  const { pagerType, avoidEmptyQuery } = useContext(AppConfigContext);
  return { pagerType, avoidEmptyQuery };
};

export default useAppConfig;
