import { useContext } from "react";

import { AppConfigContext } from "./configProvider";

const useAppId = () => {
  const { appId } = useContext(AppConfigContext);
  return appId;
};

export default useAppId;
