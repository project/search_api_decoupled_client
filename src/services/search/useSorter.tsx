import { paramsService } from "@services";
import { useCallback, useMemo } from "react";

const useSorter = () => {
  const [searchParams, setSearchParams] = paramsService.useParams();

  const currentSorting = useMemo(() => {
    return {
      sortBy: searchParams.get("sort"),
      orderBy: searchParams.get("order"),
    };
  }, [searchParams]);

  const removeSorting = useCallback(() => {
    searchParams.delete("sort");
    searchParams.delete("order");
    searchParams.delete("page");
    setSearchParams(searchParams);
  }, [searchParams, setSearchParams]);

  const setSorting = useCallback(
    (sortBy: string, orderBy: string) => {
      if (orderBy === "inactive") {
        removeSorting();
      } else {
        searchParams.set("sort", sortBy);
        searchParams.set("order", orderBy);
        searchParams.delete("page");
        setSearchParams(searchParams);
      }
    },
    [searchParams, setSearchParams, removeSorting]
  );

  return {
    currentSorting,
    setSorting,
  };
};

export default useSorter;
