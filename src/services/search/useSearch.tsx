import { apiService, configService } from "@services";

import useResults from "./useResults";
import useFacets from "./useFacets";
import useFilters from "./useFilters";
import useSorter from "./useSorter";

const useSearch = (props?: {
  facetKey?: string;
  facetAlias?: string;
  filterName?: string;
}) => {
  const { facetKey, facetAlias, filterName } = props ?? {};
  const appId = configService.useAppId();

  const {
    data: searchResponse,
    isError,
    isLoading,
    isFetching,
    refetch,
    fetchNextPage,
  } = apiService.useSearchApi(appId ?? "default");

  const {
    searchResults,
    nOfResults,
    timeTaken,
    allResults,
    pageSize,
    nOfPages,
    setPage,
    facets,
    currentPage,
    currentPageResponse,
    resetSearch,
  } = useResults({
    searchResponse,
    fetchNextPage,
  });

  const {
    facetState,
    setFacetFilter,
    setRangeFacetFilter,
    resetFacets,
    resetFacetByKey,
    currentActiveFacets,
  } = useFacets({ facetKey, facetAlias, facets });

  const { filterState, setExposedFilter, currentTextKeywords, setTextFilter } =
    useFilters({ filterName });

  const { setSorting, currentSorting } = useSorter();

  return {
    // RESULTS
    searchResults,
    currentPageResponse,
    allResults,
    nOfResults,
    isLoading,
    isFetching,
    isError,
    timeTaken,
    // REFRESH
    refetch,
    // RESET
    resetSearch,
    // FILTERS
    filterState,
    setExposedFilter,
    currentTextKeywords,
    setTextFilter,
    // FACETS
    facetState,
    setFacetFilter,
    setRangeFacetFilter,
    currentActiveFacets,
    resetFacets,
    resetFacetByKey,
    // SORTING
    setSorting,
    currentSorting,
    // PAGINATION
    currentPage: Number(currentPage),
    pageSize,
    nOfPages,
    setPage,
  };
};

export default useSearch;
