import { useMemo } from "react";

import { paramsService } from "@services";
import { type OperatorSettingsType } from "@types";
import { setExposedFilterParams } from "@utils";

const useFilters = (value: { filterName?: string }) => {
  const { filterName } = value;
  const [searchParams, setSearchParams] = paramsService.useParams();

  // Exposed filters
  const filterState = useMemo(() => {
    if (!filterName) {
      return;
    }

    const filterValue = searchParams.get(filterName);
    if (filterValue) {
      return { value: filterValue, min: undefined, max: undefined };
    } else {
      const min = searchParams.get(`${filterName}[min]`);
      const max = searchParams.get(`${filterName}[max]`);
      if (min ?? max) {
        return { min, max, value: undefined };
      }
    }
  }, [filterName, searchParams]);

  const setExposedFilter = (values: {
    from?: string;
    to?: string;
    value?: string;
    operator?: OperatorSettingsType;
  }) => {
    if (!filterName) {
      console.info("Filter name does not exist!");
      return;
    }
    // update search params
    setExposedFilterParams(searchParams, filterName, values);
    searchParams.delete("page");
    setSearchParams(searchParams);
  };

  // Text filter
  const currentTextKeywords = searchParams.get("q") ?? "";
  const setTextFilter = (value?: string) => {
    if (!value) {
      searchParams.delete("q");
    } else {
      searchParams.set("q", value);
    }
    searchParams.delete("page");
    setSearchParams(searchParams);
  };

  return {
    // Exposed filters
    filterState,
    setExposedFilter,
    // Text filter
    currentTextKeywords,
    setTextFilter,
  };
};

export default useFilters;
