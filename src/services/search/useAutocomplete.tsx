import { apiService } from "@services";

const useAutocomplete = (keywords: string) => {
  const parsedKeywords = keywords.replace?.(" ", "+");

  const { data: suggestions } = apiService.useAutocompleteApi(parsedKeywords);

  return {
    // recommendations
    suggestions,
  };
};

export default useAutocomplete;
