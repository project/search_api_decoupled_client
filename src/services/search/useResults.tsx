import { useCallback, useMemo } from "react";
import {
  type InfiniteData,
  type FetchNextPageOptions,
} from "@tanstack/react-query";

import { type SearchResponseType, type SearchResultType } from "@types";
import { configService, paramsService } from "@services";

const useResults = (props: {
  searchResponse?: InfiniteData<SearchResponseType>;
  fetchNextPage: (options?: FetchNextPageOptions | undefined) => any;
}) => {
  const [searchParams, setSearchParams] = paramsService.useParams();
  const appKey = configService.useAppKey();

  const { pagerType } = configService.useAppConfig();

  const loadMorePage = searchParams.get("pageIndex");
  const paramPage = searchParams.get("page");

  const currentPage = useMemo(() => {
    // loadmore pager always starts from page 0,
    // it should ignore the page param from the url,
    // and it should use context instead
    if (pagerType === "loadmore") {
      return loadMorePage ?? "0";
    } else {
      return paramPage ?? "0";
    }
  }, [loadMorePage, pagerType, paramPage]);

  // The current page data based on the currentPage param
  const currentPageResponse = useMemo(() => {
    return props.searchResponse?.pages.find(
      (page) => page?.search_results_page === Number(currentPage)
    );
  }, [currentPage, props.searchResponse?.pages]);

  // When changing the page, we get "currentPageResponse = undefined" until the query returns a response.
  // In that case we want to keep using the last page's response until the new page response is available.
  const infoReferencePage = useMemo(() => {
    if (currentPageResponse) {
      return currentPageResponse;
    }
    if (props.searchResponse?.pages.length) {
      return props.searchResponse.pages[props.searchResponse.pages.length - 1];
    }
  }, [currentPageResponse, props.searchResponse?.pages]);

  // Find a list of unique page numbers like [0, 1, 4].
  // In some scenarios, searchResponse?.pages can contain pages with the same page number.
  const uniquePageNumbers = useMemo(() => {
    // Find all page numbers from searchResponse?.pages
    const pageNumbers: number[] =
      props.searchResponse?.pages.map((r) => r?.search_results_page) ?? [];

    // Only keep the unique page numbers
    const uniquePageNumbers = pageNumbers.filter((item, pos) => {
      return pageNumbers.indexOf(item) === pos;
    });

    // return the sorted list
    return uniquePageNumbers.sort((a, b) => a - b);
  }, [props.searchResponse?.pages]);

  // Create a cumulative list of all results from all pages for use with the loadmore pager.
  const allResults = useMemo(() => {
    const finalRes: SearchResultType[] = [];

    uniquePageNumbers.forEach((pageNumber) => {
      // Find duplicate pages
      const samePages =
        props.searchResponse?.pages.filter(
          (p) => p?.search_results_page === pageNumber
        ) ?? [];
      const lastObject = samePages[samePages.length - 1];

      // Only add the last object of the same pages
      finalRes.push(...(lastObject?.search_results ?? []));
    });

    return finalRes;
  }, [props.searchResponse?.pages, uniquePageNumbers]);

  // provide results data
  const searchResults = infoReferencePage?.search_results ?? [];
  const nOfResults = infoReferencePage?.search_results_count ?? 0;
  const timeTaken = infoReferencePage?.took ?? 0;

  const resetSearch = () => {
    const emptyParams = new URLSearchParams();
    setSearchParams(emptyParams);
    // we wait and double check in order to avoid other hook instances to use the old searchParams
    setTimeout(() => {
      setSearchParams(emptyParams);
    }, 1);
  };

  // pagination
  const pageSize = Number(infoReferencePage?.search_results_per_page ?? 10);
  const nOfPages = Number(infoReferencePage?.search_results_pages ?? 1);

  const { fetchNextPage } = props;
  const setPage = useCallback(
    (page: string, mode: "pager" | "loadmore") => {
      if (mode === "pager") {
        // Only update the page param in the URL if we have a pager.
        // If we do it for loadmore, we lose other pages from the infinite query
        searchParams.set("page", page);
        setSearchParams(searchParams);

        const appBlock = document.getElementById(appKey ?? "default");
        const extraOffset = 25;
        window.scrollTo({
          top: (appBlock?.offsetTop ?? 0) - extraOffset,
          left: 0,
          behavior: "smooth",
        });
      } else if (mode === "loadmore") {
        searchParams.set("pageIndex", page);
        void fetchNextPage({ pageParam: searchParams });
      }
    },
    [appKey, fetchNextPage, searchParams, setSearchParams]
  );

  const facets = infoReferencePage?.facets;

  return {
    searchResults,
    currentPageResponse,
    nOfResults,
    timeTaken,
    allResults,
    pageSize,
    currentPage,
    nOfPages,
    setPage,
    facets,
    resetSearch,
  };
};

export default useResults;
