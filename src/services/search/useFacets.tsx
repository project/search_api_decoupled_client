import { useCallback, useMemo } from "react";

import { paramsService } from "@services";
import { type SearchFacetsType } from "@types";

const useFacets = (value: {
  facetKey?: string;
  facetAlias?: string;
  facets: SearchFacetsType[] | undefined;
}) => {
  const { facetKey, facetAlias, facets } = value;
  const [searchParams, setSearchParams] = paramsService.useParams();

  // provide facet helpers
  const facetState = useMemo(() => {
    if (!facetKey) {
      return;
    }

    return facets?.find((facet) => facet.key === facetKey);
  }, [facetKey, facets]);

  const setFacetFilter = (values?: string[]) => {
    if (!facetAlias) {
      console.info("Facet alias does not exist!");
      return;
    }

    // ? url param should be like: ?f[<facetAlias>]=<facetAlias>:<value>
    const activeFacetKeys: string[] = [];
    values?.forEach((value, index) => {
      const facetKey = `f[${facetAlias}_${index}]`;
      activeFacetKeys.push(facetKey);
      searchParams.set(facetKey, `${facetAlias}:${value}`);
    });
    // remove non active filters
    Array.from(searchParams.keys())
      .filter(
        (key) =>
          key.startsWith(`f[${facetAlias}_`) && !activeFacetKeys.includes(key)
      )
      .forEach((key) => {
        searchParams.delete(key);
      });

    searchParams.delete("page");
    setSearchParams(searchParams);
  };

  const setRangeFacetFilter = (values: { min?: string; max?: string }) => {
    if (!facetAlias) {
      console.info("Facet alias does not exist!");
      return;
    }

    // ? url param should be like: ?f[<facetAlias>]=<facetAlias>:(min:<min>,max:<max>)
    const facetKey = `f[${facetAlias}]`;
    if (!values.min && !values.max) {
      searchParams.delete(facetKey);
    } else {
      const facetValue = `${facetAlias}:(min:${values.min ?? ""},max:${
        values.max ?? ""
      })`;
      searchParams.set(facetKey, facetValue);
    }

    searchParams.delete("page");
    setSearchParams(searchParams);
  };

  const getActiveFacetLabels = useCallback((facet: SearchFacetsType) => {
    const firstActiveValue = facet?.active_values?.[0];
    if (
      // check if it's an array of 2 elements, not a string (Range)
      Array.isArray(firstActiveValue) &&
      firstActiveValue.length === 2
    ) {
      // range value
      return [
        `${facet?.active_values?.[0]?.[0] ?? ""} - ${
          facet?.active_values?.[0]?.[1] ?? ""
        }`,
      ];
    } else {
      // normal multiple value facet
      return (
        facet?.active_values?.map(
          (activeValue) =>
            facet.results.find((result) => result.key === activeValue)?.label
        ) ?? []
      );
    }
  }, []);

  const currentActiveFacets = useMemo(
    () =>
      facets
        ?.filter((facet) => facet.active_values?.length)
        .map((facet) => ({
          label: facet.label,
          active_values: getActiveFacetLabels(facet),
          key: facet.key,
        })),
    [facets, getActiveFacetLabels]
  );

  const resetFacets = () => {
    facets?.forEach((facet) => {
      if (facet.active_values?.length) {
        facet.active_values?.forEach((activeValue, index) => {
          searchParams.delete(`f[${facet.key}_${index}]`);
        });
      }
    });
    setSearchParams(searchParams);
  };

  const resetFacetByKey = (facetKey: string) => {
    const facet = facets?.find((facet) => facet.key === facetKey);

    if (facet?.key) searchParams.delete(`f[${facet.key}]`);
    if (facet?.active_values?.length) {
      facet.active_values?.forEach((_, index) => {
        searchParams.delete(`f[${facet.key}_${index}]`);
      });
    }
    setSearchParams(searchParams);
  };

  return {
    // General Facets
    facetState,
    setFacetFilter,
    resetFacetByKey,
    // Range Facets
    setRangeFacetFilter,
    // All
    currentActiveFacets,
    resetFacets,
  };
};

export default useFacets;
