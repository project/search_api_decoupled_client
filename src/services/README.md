### _src/services_

## Services folder

We use this folder to organise all the services in our project.

## Logic behind this folder structure

For this project we have the following structure:

- **api**: provides endpoint calls and data
- **config**: provides search configurations handling
- **mocks**: provides mocked content for development purposes
- **params**: provides search params handling and storing
- **search**: provides search queries and responses handling
