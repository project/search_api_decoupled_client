import { useQuery } from "@tanstack/react-query";
import { type AxiosError } from "axios";

import { type AutocompleteResponseType } from "@types";

import api from "./api";
import useBaseUrl from "./useBaseUrl";

const useAutocompleteApi = (keywords: string) => {
  const { host, baseUrlAutocomplete } = useBaseUrl();

  const queryId = ["autocomplete", keywords];
  const { data, isError, isLoading, refetch } = useQuery<
    AutocompleteResponseType,
    AxiosError
  >(queryId, async () => {
    if (baseUrlAutocomplete) {
      return await api
        .requestAutocomplete(host, baseUrlAutocomplete, keywords)
        .then((res) => res.data);
    }
  });
  return {
    data,
    isError,
    isLoading,
    refetch,
  };
};

export default useAutocompleteApi;
