import { useInfiniteQuery } from "@tanstack/react-query";
import { type AxiosError } from "axios";

import { paramsService, configService } from "@services";
import { type SearchResponseType } from "@types";

import api from "./api";
import useBaseUrl from "./useBaseUrl";

const useSearchApi = (appKey: string) => {
  const [searchParams] = paramsService.useParams();
  const { pagerType, avoidEmptyQuery } = configService.useAppConfig();
  const { host, baseUrl } = useBaseUrl();
  const filteredSearchParams = new URLSearchParams(searchParams);
  filteredSearchParams.delete("pageIndex");
  const queryId = ["search", appKey, baseUrl, filteredSearchParams.toString()];

  const { data, isError, isLoading, isFetching, refetch, fetchNextPage } =
    useInfiniteQuery<SearchResponseType, AxiosError>({
      queryKey: queryId,
      // `value` is the input of the `fetchNextPage` method
      queryFn: async ({ pageParam }) => {
        const localSearchParams = new URLSearchParams(
          (pageParam as URLSearchParams | undefined) ?? searchParams
        );

        // Get the in-app pageIndex (usually used for loadmore pager)
        const pageIndex = localSearchParams?.get("pageIndex");
        localSearchParams.delete("pageIndex");

        // Get the page number from the url
        const pageNumberFromUrl = localSearchParams?.get("page");
        // Initial page is 0
        let page = "0";

        if (pageIndex) {
          // We already know the index of the next page, no need to check the URL
          page = pageIndex;
        } else if (pagerType !== "loadmore" && pageNumberFromUrl) {
          // The page has been loaded with a page number in the url
          page = pageNumberFromUrl;
        }

        // Update params with the correct page number
        const params = new URLSearchParams({
          ...{
            ...(localSearchParams ? Object.fromEntries(localSearchParams) : {}),
          },
          // if page is 0 we don't need to add it to the url
          ...(page !== "0" && { page }),
        });

        if (baseUrl && !(avoidEmptyQuery && !params.get("q"))) {
          return await api
            .requestSearch(host, baseUrl, params)
            .then((res) => res.data);
        }
      },
    });

  return {
    data,
    isError,
    isLoading,
    isFetching,
    refetch,
    fetchNextPage,
  };
};

export default useSearchApi;
