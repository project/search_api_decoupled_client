import React, { type ReactNode } from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

const SearchApiQueryProvider = (props: { children: ReactNode }) => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        retry: false,
        staleTime: 1000 * 60 * 5,
        keepPreviousData: true,
        cacheTime: 0, // disabling the cache because the infinite query returns wrong pages with caching and after changing the facets multiple times
      },
    },
  });

  return (
    <QueryClientProvider client={queryClient}>
      {props.children}
    </QueryClientProvider>
  );
};

export default SearchApiQueryProvider;
