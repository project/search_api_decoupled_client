import { useQuery } from "@tanstack/react-query";
import { type AxiosError } from "axios";

import { type EndpointConfigType } from "@types";

import api from "./api";

const useConfigApi = (host?: string, id?: string, skip?: boolean) => {
  const queryId = ["config", host, id];
  const { data, isError, isLoading, refetch } = useQuery<
    EndpointConfigType,
    AxiosError
  >(
    queryId,
    async () => {
      if (!skip && host && id) {
        return await api.requestConfig(host, id).then((res) => res.data);
      }
    },
    {
      cacheTime: 60 * 5, // 5 minutes
    }
  );
  return {
    data,
    isError,
    isLoading,
    refetch,
  };
};

export default useConfigApi;
