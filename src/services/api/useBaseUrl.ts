import { configService } from "@services";

const useBaseUrl = () => {
  const { appConfig } = configService.useConfig();
  return {
    host: appConfig?.host ?? "",
    baseUrl: appConfig?.base_url,
    baseUrlAutocomplete: appConfig?.base_url_autocomplete,
  };
};

export default useBaseUrl;
