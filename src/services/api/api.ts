import axios from "axios";

const api = {
  requestConfig: async (host: string, id: string) =>
    await axios.get(`${host}/api/search/${id}/ui-settings`, {
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
      },
    }),
  requestSearch: async (
    host: string,
    baseUrl: string,
    searchParams?: URLSearchParams
  ) =>
    await axios.get(
      `${host + baseUrl.replace(host, "")}?${searchParams?.toString() ?? ""}`,
      {
        headers: {
          "Content-Type": "application/json",
          accept: "application/json",
        },
      }
    ),
  requestAutocomplete: async (host: string, baseUrl: string, text: string) =>
    await axios.get(`${host + baseUrl.replace(host, "")}?q=${text}`),
};

export default api;
