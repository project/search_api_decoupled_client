import {
  type ConfigElementType,
  type ExposedFilterSettingsType,
  type SorterSettingsType,
} from "@types";
import { setExposedFilterParams } from "@utils";
import { useConfig } from "../config";

const useDefaultParams = () => {
  const config = useConfig();

  const defaultSearchParams = new URLSearchParams();

  // Default sorting
  const sorter = config.appElements?.find(
    (element) => element.type === "sorting"
  ) as ConfigElementType<SorterSettingsType>;
  const defaultSort = sorter?.settings.default;
  const defaultOrder = sorter?.settings.info?.[defaultSort]?.default_sort_order;
  if (defaultSort) {
    defaultSearchParams.set("sort", defaultSort);
  }
  if (defaultOrder) {
    defaultSearchParams.set("order", defaultOrder);
  }

  // Default exposed filters
  const exposedFilters = config.appElements?.filter(
    (element) => element.type === "exposed_filter"
  ) as Array<ConfigElementType<ExposedFilterSettingsType>>;
  exposedFilters?.forEach((filter) => {
    const defaultValues =
      filter.settings.operator === "between"
        ? {
            from: filter.settings.default_value,
            to: filter.settings.default_value_2,
          }
        : {
            value: filter.settings.default_value,
          };
    if (filter.settings.field_name) {
      setExposedFilterParams(defaultSearchParams, filter.settings.field_name, {
        operator: filter.settings.operator ?? "equal",
        ...defaultValues,
      });
    }
  });

  return defaultSearchParams;
};

export default useDefaultParams;
