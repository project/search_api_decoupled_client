import { useContext, useEffect, useMemo } from "react";
import { useSearchParams } from "react-router-dom";

import { SearchParamsContext } from "./paramsProvider";
import { useConfig } from "../config";
import useDefaultParams from "./useDefaultParams";

type ParamsType = [
  searchParams: URLSearchParams,
  setSearchParams: (searchParams: URLSearchParams) => void
];

const useParams = (): ParamsType => {
  const { stateManager } = useConfig();
  const [urlParams, setUrlParams] = useSearchParams();
  const { contextParams, setContextParams } = useContext(SearchParamsContext);

  const [searchParams, setSearchParams] = useMemo(() => {
    switch (stateManager) {
      case "context":
        return [contextParams, setContextParams];
      case "url":
      default:
        return [urlParams, setUrlParams];
    }
  }, [stateManager, contextParams, setContextParams, urlParams, setUrlParams]);

  // Ensure default search params are set
  const defaultSearchParams = useDefaultParams();
  useEffect(() => {
    if (
      searchParams.toString() === "" &&
      defaultSearchParams.toString() !== ""
    ) {
      setSearchParams(defaultSearchParams);
    }
  }, [defaultSearchParams, searchParams, setSearchParams]);

  return [searchParams, setSearchParams];
};

export default useParams;
