### _src/store_

## Params folder

We use this folder to organise all logics and codes related to state / url store, actions, etc.

For this project we will use context API and the Router search params to store the state of the application depending on the case.
