import React, {
  type ReactNode,
  createContext,
  useState,
  useCallback,
} from "react";

const SearchParamsContext = createContext({
  contextParams: new URLSearchParams(),
  setContextParams: (searchParams: URLSearchParams) => {
    console.log(searchParams.toString());
  },
});

const SearchParamsContextProvider = (props: { children: ReactNode }) => {
  const [contextParams, tempSetContextParams] = useState(new URLSearchParams());

  const setContextParams = useCallback(
    (tempsearchParams: URLSearchParams) => {
      tempSetContextParams(new URLSearchParams(tempsearchParams));
    },
    [tempSetContextParams]
  );

  return (
    <SearchParamsContext.Provider value={{ contextParams, setContextParams }}>
      {props.children}
    </SearchParamsContext.Provider>
  );
};

export { SearchParamsContext, SearchParamsContextProvider };
