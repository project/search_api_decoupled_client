import React from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";

import { paramsService, apiService } from "@services";
// KEEP THIS UNUSED IMPORT. It injects drupal functions in dev mode
// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
import { drupal } from "@utils";

import { AppFactory } from "@components/factories";

import reportWebVitals from "./reportWebVitals";

export const injectApp = (host?: string) => {
  if (process.env.NODE_ENV === "development" && process.env.MOCK) {
    import("@mocks")
      .then(({ MockSettingsPortal, MockConfigContextProvider }) => {
        const mockContainer = document.body.appendChild(
          document.createElement("DIV")
        );
        const mockRoot = createRoot(mockContainer);
        mockRoot.render(
          <React.StrictMode>
            <MockConfigContextProvider>
              <MockSettingsPortal />
            </MockConfigContextProvider>
          </React.StrictMode>
        );
      })
      .catch((e) => {
        console.info("No mock settings portal found");
      });
  }

  const container = document.body.appendChild(document.createElement("DIV"));
  const root = createRoot(container);

  let config: any;
  // update app factory key to trigger updates
  let counter = 0;
  const renderInterval = setInterval(() => {
    if (!config || window.drupalSettings?.search_api_endpoint !== config) {
      counter++;
      config = window.drupalSettings?.search_api_endpoint;
      root.render(
        <React.StrictMode>
          <BrowserRouter>
            <paramsService.SearchParamsContextProvider>
              <apiService.SearchApiQueryProvider>
                <AppFactory key={counter} />
              </apiService.SearchApiQueryProvider>
            </paramsService.SearchParamsContextProvider>
          </BrowserRouter>
        </React.StrictMode>
      );
    }
    if (process.env.NODE_ENV !== "development") {
      // on prod we don't need to keep checking for changes
      clearInterval(renderInterval);
    }
  }, 300);
};

injectApp();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
