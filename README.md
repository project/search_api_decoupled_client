# Search API decoupled / client

This app is meant to be UI agnostic search client for Search API decoupled module.

It's built with React and Typescript. And it expects to get layouts, regions, config, custom elements and results from the backend.

The app expects 3 interfaces:

- `window.drupalSettings.search_api_endpoint`: for the ui and app config
- the layout markup: for the region portals and rendering
- the custom elements: attached separately as a library that reads the entrypoints and renders widgets

The rendering of elements is done using the tags provided by the backend or divs, on production, this app is totally independent and only expects the config, the backend responses and the layout markup.

The app will be used in the `search api decoupled ui` drupal module in order to consume the search api decoupled backend module, anyhow, it could also be used in decoupled websites as a static asset, config should still be provided via window variable.

> More documentation can be found in the wiki for the project: <https://git.drupalcode.org/project/search_api_decoupled_client/-/wikis/home>

## Local Setup

You only need to follow these steps to get the app running in your local machine.

1. Cloning the monorepo:
   - `git clone git@git.drupal.org:project/search_api_decoupled_client.git`
2. For development, you should use a UI library:
   - by default `@search-api-decoupled/ui` is used
   - soon we will also use `@search-api-decoupled/wc-ui` as a default custom element library
   - if you want to use another library change the dependency in `package.json` and ensure you add the required js and css files in `public/index.html` so they are available during development
   - if you are using custom element library update the mocks in `elements.ts` or use the `decoupled` layout on the development server settings
3. Install app dependencies:
   - `nvm use && npm i`
4. Development server:
   - we use mocked environments, there are npm commands offering the following:
     - use `npm run dev` to mock the config (RECOMMENDED) (you can adapt it in `elements.ts` and `layouts.ts`)
     - use `npm run start` to run the app without mocked config, you will need to set the window variable `drupalSettings.search_api_endpoint` with the config you want to use
   - you will be able to access your app under <http://localhost:3000/>
5. Development tools
   - the development settings modal will be prompted by default until you specify the host to use as backend
   - the host can be local or external, but should have CORS open for the app to work
   - you can use the development tools in order to change host, layout, input, pager, results widgets and so on at any moment
