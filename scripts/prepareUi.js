const fs = require("fs");
const fse = require("fs-extra");
const { glob } = require("glob");

process.env.NODE_ENV = "production";

console.log("\x1b[36m%s\x1b[0m", "\n\n\n\n\n\nPOST INSTALL IS RUNNING:");

// PREPARE @search-api-decoupled/ui scripts

const simpleUIScripts = ["injector.js"];
const simpleUIPath = "node_modules/@search-api-decoupled/ui/dist/";
const simpleUIComponentsPath = simpleUIPath + "components/*.js";

const publicPath = "public/";
const publicComponentsPath = publicPath + "components/";
const publicAssetsPath = publicPath + "assets/";


// create /components and /assets folders in public if they don't exist
if (!fs.existsSync(publicComponentsPath)) {
  fs.mkdirSync(publicComponentsPath);
}
if (!fs.existsSync(publicAssetsPath)) {
  fs.mkdirSync(publicAssetsPath);
}

glob(simpleUIComponentsPath)
  .then((files) => {
    console.info(">>> Moving Simple UI files to the public folder");
    let componentsScripts = [];
    let componentsStyles = [];
    files.forEach((file) => {
      const i = file.indexOf("/components/");
      const name = file.slice(i + 12);
      const newPath = `${publicComponentsPath}${name}`;
      console.log(`Copying ${file} to ${newPath}`);
      fs.copyFileSync(file, newPath);

      componentsScripts.push(
        `    <script src="${newPath}" type="module"></script>\n`.replace(
          "public/",
          "%PUBLIC_URL%/"
        )
      );

      componentsStyles.push(
        `    <link rel="stylesheet" href="public/assets/css/${name.split('.')[0]}.css">\n`.replace(
          "public/",
          "%PUBLIC_URL%/"
        )
      );
    });

    console.info(">>> Copy the other required files: ", simpleUIScripts);

    simpleUIScripts.forEach((file) => {
        const oldPath = `${simpleUIPath}${file}`;
        const newPath = `${publicPath}${file}`;
        console.log(`Copying ${oldPath} to ${newPath}`);
        fs.rmSync(newPath, { force: true });
        fs.copyFileSync(oldPath, newPath);
      }
    );

    const allInjectableList = [...componentsScripts.reverse(), ...componentsStyles.reverse()]

    console.info(">>> Writing scripts into the HTML file");
    const htmlData = fs.readFileSync("public/index.html", "utf-8");
    const regex =
      /(.*<!-- Start of Simple UI's links -->)([\s\S]*)(.*<!-- End of Simple UI's links -->)/gm;
    const newData = htmlData.replace(
        regex,
        "$1\n" + allInjectableList.join("") + "\n    $3"
        );
    fs.writeFileSync("public/index.html", newData, "utf-8");
        
    console.info(">>> Copying assets folder into the public folder");
    fse.copySync(simpleUIPath + "assets", publicPath + "assets", {
        overwrite: true,
    });
    })
.catch((err) => {
    console.error(">>> An error happened during the post install: ", err);
});

// TODO: PREPARE custom-library scripts