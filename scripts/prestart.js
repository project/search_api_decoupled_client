const fse = require('fs-extra');
const glob = require('glob');

console.log('\x1b[36m%s\x1b[0m', '\n\n\n\n\n\nPre start script IS RUNNING:');

// Copying the readme file to the src folder so we can use it inside the home page
fse.copy('README.md', 'src/README-tmp.md', (err) => {
    if (err) throw err;
    console.log('README.md was copied to src/README-tmp.md');
});
