# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# @search-api-decoupled/client

## not released yet

## [2.5.4] - 2024-07-09

### Fixed

- fix infinite state loop when the default params are empty (clean search)

## [2.5.3] - 2024-05-29

### Fixed

- fix load-more page concatenation with context params, queryID was taking the pageIndex as part of the key and forgetting about older pages

## [2.5.2] - 2024-04-23

### Fixed

- fix re-rendering issues on rendered results due to key passed to SanitizedHTML component

## [2.5.1] - 2024-04-16

### Fixed

- fix range higher/lower attribute calculation, ensuring it can be undefined when data is not enough to properly calculate it

## [2.5.0] - 2024-04-12

### Added

- support a new range facet min/max type called "stats" where values are calculated in the backend and provided as results

## [2.4.5] - 2024-04-02

### Fixed

- ensure exposed filter url gets correclty updated on reset and params removed when they are passed as undefined or not passed at all

## [2.4.4] - 2024-03-27

### Fixed

- avoid undefined sorter default values

### Changed

- debounce range changes for the facet interface

## [2.4.3] - 2024-03-12

### Fixed

- fix sorter default values naming

## [2.4.2] - 2024-03-12

### Fixed

- fix default search params approach, separating them and setting them as default values for the search params hook instead of manually setting them for each component on render time

## [2.4.1] - 2024-03-11

### Fixed

- change range (min,max) attributes back to (lower,higher) for the rendered component

## [2.4.0] - 2024-03-08

### Added

- implement not_empty app config, this enables the app to avoid searching until some text input is provided
- wrapper abstraction, with `useWrapperAttributes` and other utils
- implement `useCustomEventListeners` hook to simplify and abstract how the listeners are added and removed
- implement different min/max range types, fixed (existing one) and search_results (new one), depending on where the min/max values are coming from

### Changed

- adjut naming for variables, app config context and other minor tweaks
- minor simplifications and renaming for the codebase
- simplify element settings types
- rename `show_title` to `show_label` for the received wrapper settings from the api

### Fixed

- correctly handle boolean for exposed filter ressetable param

## [2.3.1] - 2024-03-01

### Fixed

- fix reset interface, ensuring the `reset-search` event is cleaning the whole search state
- fix exposed filter operator url format
- make range labels translateable by Drupal

## [2.3.0] - 2024-02-27

### Fixed

- fix exposed filter default values
- fix autocomplete suggestion snippets

### Added

- add sorter default values

### Changed

- exposed filter interface for "between" operator is now rendering only 1 element instead of 2
- exposed filter interface is now always rendering 1 element with attributes: `value`, `operator`, `from-value` and `to-value`

## [2.2.0] - 2024-02-22

### Added

- Add support for `stateManager` which offers the option of using url or context as state managers

### Fixed

- Fix usage of `context` state manager

## [2.1.0] - 2024-02-19

### Changed

- ensure exposed filters set their default values and they are used on every request

### Fixed

- fix active filters interface minor issues
- ensure active range facets are properly removed when active filter is pressed (keys issue)

## [2.0.0] - 2024-02-14

### Changed

- refactor rendering method, use react elements instead of dangerouslySetInnerHTML on divs, this improves performance and allows react to keep track of the rendered elements, avoiding flickers and other issues
- change facetId to facetKey in order to promote consistency

## Fixed

- minor adjustments on the input and autocomplete interfaces in order to fix initial value and suggestions

## [1.2.0] - 2024-01-07

### Added

- implement range-facet support in the facet interface
- implement sorting element interface
- implement active-facets element interface

### Changed

- improve facet type definition
- improved exposed filters interface, naming and actions

## [1.1.2] - 2023-01-23

### Fixed

- fix `pagerType` recognition for the `loadmore` element type (before it was a widget)

## [1.1.1] - 2023-12-28

### Changed

- adjust exposed filters and support more operators

## [1.1.0] - 2023-12-27

### Added

- hierarchy support for facets
- exposed filters support

### Changed

- improve dev env settings
  - add endpoint name support
- improve and simplify interfaces and factories
- improve and simplify folder structure

## [1.0.11] - 2023-10-11

### Fixed

- extend sanitizer attributes instead of replacing the default ones

## [1.0.10] - 2023-10-10

### Changed

- fix custom results attribute mapping
- make switch configurable for custom and switch rendered results

## [1.0.9] - 2023-10-09

### Changed

- handle "not string" preview_image values so they do not break the app
- encode text attribute for results
- handle production post install, now using prepare script which only runs in development
- handle undefined img for custom results

## [1.0.8] - 2023-10-06

### Changed

- update block id selector from id to data-block-id
- update mocked elements and default input widget

## [1.0.7] - 2023-10-06

## [1.0.6] - 2023-10-06

### Changed

- Bump @search-api-decoupled/ui to v1.0.2

## [1.0.5] - 2023-10-06

### Fixed

- rendered results handling (using element approach)

## [1.0.4] - 2023-10-06

### Fixed

- client injection

### Added

- Small hint for the development settings host, specifying host requirements
- Improve development experience, elements, disabled widgets, layouts ...

## [1.0.3] - 2023-10-05

### Added

- setup qaack for test envs

### Changed

- improved documentation and readmes

### Fixed

- prepareUI script

## [1.0.2] - 2023-09-28

### Added

- Initial release including all the internal developed features

### Changes

- Rebranding to `@search-api-decoupled/client`
- Independence from 1x internal project styles and markup

# xsearch (1xInternet internal project)

## [2.3.0] - 2023-09-28

### Added

- Implement div based ui support, now the app can render divs with attributes when no customElements is specified by the backend

### Removed

- Tailwind and Quartz style and markup dependencies, now the app dev env has its own styles and markup
- tailwind dependency

## [2.2.2] - 2023-09-26

### Fixed

- Fix host handling after decoupled config retrieval

## [2.2.1] - 2023-09-26

### Fixed

- Fix decoupled config error, too many re-renders due to updating state inconsistently

### Added

- Improve development experience for decoupled, with mocked layout option

## [2.2.0] - 2023-09-21

### Added

- Decoupled config retrieval, now only providing `host` and `id` from `window.drupalSettings.search_api_endpoint.x` the app takes care of retrieving the rest of the config from the backend
- Handle absolute paths for the custom search results

### Fixed

- Change reset and refresh settings type and use the label from there

## [2.1.1] - 2023-09-18

### Fixed

- handle undefined window variable

## [2.1.0] - 2023-09-18

### Changed

- improve `facet` interface
- update mocked elements
- avoid mocks to be bundled for production
- adjust root rendering to wait for settings to be available

### Added

- new interfaces for `refresh` and `reset` elements

## [2.0.0] - 2023-08-25

### Changed

- improve `loadmore` feature and page param handling
- minor hook adjustments and structure improvements
- typing improvements
- re-architecture of the app, converting it in a ui-agnostic search renderer
- define interfaces for the widget customElements
- implement customElement template

## [1.5.0] - 2023-07-10

### Added

- Add `loadmore` widget
- Add polyfill for process.env
- Add a new script for the `loadmore` widget: `start:mock-config-loadmore`
- New develoment tools and mock settings features for development experience

### Changed

- Use `useInfiniteQuery` instead of `useQuery` for the search results
- Use `base_url_autocomplete` from `drupalSettings` in the autocomplete search endpoint instead of custom hardcoded url
- Improve mocks system, now we have development tools in the react dev server header that we can use to actually change the layout, pager, input and results widgets when working with mocks in development, using the dxp backend

### Fixed

- Change text facet key from `text` to `q`
- Reset the page number when the search is performed or facet filters are applied
- Adjust loader, remove text, make it linear and ensure absolute positioning

## [1.4.0] - 2023-06-06

### Added

- `SanitizedHTML` component for the dangerous html strings received from the backend (hook and component)
- Improved CI to publish new versions to npm on every tag
- Improved docs
- Search in different languages based on drupalSettings
- Apply the new code style

## [1.3.1] - 2023-05-24

### Fixed

- Handle initial value for the autocomplete widget
- Warning linting errors
- Add gap to the horizontal results

## [1.3.0] - 2023-05-10

### Fixed

- Adjust reset button for vertical orientations
- Fix checkboxes initial state population
- Minor visual adjustments

### Added

- Auto-search on empty input or input clear for the autocomplete widget
- `url_alias` usage in order to generate url search params

## [1.2.1] - 2023-05-03

### Fixed

- Avoid empty divs when categories are not provided

## [1.2.0] - 2023-05-03

### Changed

- Fix for the local search results, handle arrays and avoid displaying empty tags

### Added

- Add xsearch attribute to all the quartz components so their styles can be extended or modified

## [1.1.1] - 2023-05-02

### Changed

- folder structure for utilities components
- minor adjustment for the facet labels using the new `show_title` settings key
- minor adjustment for the facet reset button, now not hidden when only one bucket is available

## [1.1.0] - 2023-04-24

### Added

- Search results switch utility
- Search results switch widget
- Search results local (with switch) widget
- New layouts for the new results widgets
- New mock scripts for the new results widgets

### Changed

- Improved mocked config
- Updated types
- Update to new quartz colors

## [1.0.0] - 2023-04-12

This is the first functional version of the app implementing an MVP that can be used for the initial purposes this app is intended for.

Main tasks completed include:

- App architecture and structure
- Project linting, modules, imports, folder structure ... setup
- Factory based apps, portals and widgets
- Service based bussiness logic, separated from the ui
- Api service for api calls
- Search, config and params as main services
- Mocks service for easier development
- Building and publishing adjustments and optimizations
